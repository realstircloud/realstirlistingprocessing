﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Xml.Linq;

namespace Realstir.Lib.Parser
{
    public class DynamicXmlNode : DynamicObject
    {
        private Dictionary<string, object> _dictionary = new Dictionary<string, object>();
        private XElement _rootNode = null;

        //public DynamicXmlNode(string filePath) : this(XDocument.Load(filePath).Root)
        //{
        //}

        public DynamicXmlNode(string xmlData) : this(XDocument.Parse(xmlData).Root)
        {
        }

        public DynamicXmlNode(XElement element)
        {
            _rootNode = element;
            foreach (var item in element.Attributes())
            {
                string name = FormatName(item.Name);
                _dictionary[name] = item.Value;
                _dictionary["__" + name] = item;
            }

            //handle repeating elemtns
            var elementGroups = from e in element.Elements()
                                group e by e.Name
                                    into g
                                select new { Name = g.Key, Values = g };
            foreach (var elementGroup in elementGroups)
            {
                string name = FormatName(elementGroup.Name);
                XElement firstElement = elementGroup.Values.First();
                if (elementGroup.Values.Count() > 1)
                {
                    string arrayName = name + "__array";
                    if (elementGroup.Values.Any(e => e.HasAttributes || e.HasElements))
                    {
                        _dictionary[arrayName] = new List<DynamicXmlNode>(from i in elementGroup.Values select new DynamicXmlNode(i));
                    }
                    else
                    {
                        _dictionary[arrayName] = new List<string>(from i in elementGroup.Values select i.Value);
                    }
                    _dictionary["__" + arrayName] = elementGroup.Values;
                }
                else
                {
                    if (firstElement.HasElements || firstElement.HasAttributes)
                    {
                        _dictionary[name] = new DynamicXmlNode(firstElement);
                    }
                    else _dictionary[name] = firstElement.Value;
                    _dictionary["__" + name] = firstElement;
                }
            }
        }

        private string FormatName(XName nodeName)
        {
            return nodeName.LocalName.ToLower().Replace('.', '_');
        }

        public static implicit operator string (DynamicXmlNode x)
        {
            return x._rootNode.Value;
        }

        public override bool TryGetMember(
        GetMemberBinder binder, out object result)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            string name = binder.Name.ToLower();
            if (name == "_")
            {
                result = ToString();
                return true;
            }
            if (name == "__")
            {
                result = _rootNode;
                return true;
            }
            var splitName = name.Split(new[] { '_', '_' }, StringSplitOptions.RemoveEmptyEntries);

            if (_dictionary.ContainsKey(name))
            {
                result = _dictionary[name];
            }
            else if (splitName.Count() == 2 && splitName.Last() == "array")
            {
                if (_dictionary.ContainsKey(splitName.First()))
                    result = new List<dynamic> { _dictionary[splitName.First()] };
                else result = new List<DynamicXmlNode>();
            }
            else result = null;
            return true;
        }

        // Writing is not yet supported
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            _dictionary[binder.Name.ToLower()] = value;

            return true;
        }

        public override string ToString()
        {
            return _rootNode.Value;
        }

        //public static DynamicXmlNode LoadFile(string filePath)
        //{
        //    return new DynamicXmlNode(filePath, null);
        //}
        public static DynamicXmlNode LoadXml(string xmlData)
        {
            return new DynamicXmlNode(xmlData);
        }
    }
}