﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Reflection;
using System.ServiceProcess;

namespace HomeJunctionService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            ServiceProcessInstaller serviceProcessInstaller = GetServiceProcessInstaller();

            string serviceName = GetConfigurationValue("ServiceName");

            ServiceInstaller serviceInstaller = new ServiceInstaller();
            serviceInstaller.ServiceName = serviceName;
            serviceInstaller.Description = serviceName;

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }

        private ServiceProcessInstaller GetServiceProcessInstaller()
        {
            ServiceProcessInstaller installer = new ServiceProcessInstaller();
            installer.Account = ServiceAccount.LocalSystem;

            string serviceAccountUsername = null;
            string serviceAccountPassword = null;
            try
            {
                serviceAccountUsername = GetConfigurationValue("ServiceAccountUsername");
                serviceAccountPassword = GetConfigurationValue("ServiceAccountPassword");
            }
            catch { serviceAccountUsername = null; }

            if ((serviceAccountUsername != null) && (serviceAccountUsername.Trim().Length > 0))
            {
                installer.Account = ServiceAccount.User;
                installer.Username = serviceAccountUsername;
                installer.Password = serviceAccountPassword;

            }

            return installer;
        }

        private string GetConfigurationValue(string key)
        {
            Assembly service = Assembly.GetAssembly(typeof(ProjectInstaller));
            Configuration config = ConfigurationManager.OpenExeConfiguration(service.Location);
            if (config.AppSettings.Settings[key] != null)
            {
                return config.AppSettings.Settings[key].Value;
            }
            else
            {
                throw new IndexOutOfRangeException
                    ("Settings collection does not contain the requested key: " + key);
            }
        }
    }
}
