﻿using Realstir.Engine.Listing.Core;
using System.ServiceProcess;
using System.Threading;

namespace HomeJunctionService
{
    public partial class mainService : ServiceBase
    {
        private volatile bool _bStop = false;
        private static int _nCheckIntervalInMs = 60000;
        private static int _nSystemClientID = 0;

        private static Thread _thrdMain = null;
        private static Thread _thrdHeartbeat = null;
        private static int _nHeartbeatIntervalInMs = 60000;
        public mainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _bStop = false;

            _thrdMain = new Thread(new ThreadStart(this.StartWork));
            _thrdMain.Start();
        }

        protected override void OnStop()
        {
            doStop();
        }

        private void doStop()
        {
            _bStop = true;
            try
            {
                _thrdHeartbeat.Abort();
                _thrdHeartbeat.Join();
            }
            catch { }

            try
            {
                _thrdMain.Abort();
                _thrdMain.Join();
            }
            catch { }

        }

        public void StartWork()
        {
            LocalConfig config = new LocalConfig();
            _nCheckIntervalInMs = config.CheckIntervalInMs;
            _nSystemClientID = config.ClientID;

            _thrdHeartbeat = new Thread(new ThreadStart(this.StartWorkHeartbeat));
            _thrdHeartbeat.Start();

            HomeJunctionCore homeJunctionCore = null;

            while (!_bStop)
            {
                homeJunctionCore = new HomeJunctionCore();
                //homeJunctionCore.DownloadFeed("sandicor");
                //homeJunctionCore.ClusterFeed();
                homeJunctionCore.Upload();
                homeJunctionCore = null;
                Thread.Sleep(_nCheckIntervalInMs);
            }

        }

        public void StartWorkHeartbeat()
        {
            //BusFacCore busFacCore = null;
            //while (!_bStop)
            //{
            //    busFacCore = new BusFacCore();
            //    busFacCore.SystemClientUpdateHeartbeat(_nSystemClientID);
            //    busFacCore = null;
            //    Thread.Sleep(_nHeartbeatIntervalInMs);
            //}
        }
    }
}
