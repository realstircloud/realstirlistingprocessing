﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace HomeJunctionService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[]
            //{
            //    new mainService()
            //};
            //LocalConfig config = new LocalConfig();
            //ServicesToRun[0].ServiceName = config.ServiceName;
            //ServiceBase.Run(ServicesToRun);

            mainService srv = new mainService();
            srv.StartWork();
        }
    }
}
