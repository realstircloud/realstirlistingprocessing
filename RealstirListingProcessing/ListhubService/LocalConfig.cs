﻿using System;
using System.Configuration;

namespace ListhubService
{
    public class LocalConfig
    {
        private int _nClientID = 0;
        private int _nCheckIntervalInMs = 5000;
        private string _strServiceName = null;
        private string _strServiceAccountUsername = null;
        private string _strServiceAccountPassword = null;

        private static readonly string KEY_CLIENT_ID = "ClientID";
        private static readonly string KEY_CHECK_INTERVAL_IN_MS = "CheckIntervalInMs";
        private static readonly string KEY_SERVICE_NAME = "ServiceName";
        private static readonly string KEY_SERVICE_ACCOUNT_USERNAME = "ServiceAccountUsername";
        private static readonly string KEY_SERVICE_ACCOUNT_PASSWORD = "ServiceAccountPassword";

        public string ServiceName
        {
            get { return _strServiceName; }
            set { _strServiceName = value; }
        }
        public string ServiceAccountUsername
        {
            get { return _strServiceAccountUsername; }
            set { _strServiceAccountUsername = value; }
        }
        public string ServiceAccountPassword
        {
            get { return _strServiceAccountPassword; }
            set { _strServiceAccountPassword = value; }
        }
        public int ClientID
        {
            get { return _nClientID; }
            set { _nClientID = value; }
        }
        public int CheckIntervalInMs
        {
            get { return _nCheckIntervalInMs; }
            set { _nCheckIntervalInMs = value; }
        }

        public LocalConfig()
        {
            AppSettingsReader configurationAppSettings = null;
            configurationAppSettings = new AppSettingsReader();

            try
            {
                _nClientID = Convert.ToInt32((string)configurationAppSettings.GetValue(KEY_CLIENT_ID, typeof(System.String)));
            }
            catch { _nClientID = 0; }

            try
            {
                _nCheckIntervalInMs = Convert.ToInt32((string)configurationAppSettings.GetValue(KEY_CHECK_INTERVAL_IN_MS, typeof(System.String)));
            }
            catch { _nCheckIntervalInMs = 5000; }

            try
            {
                _strServiceName = (string)configurationAppSettings.GetValue(KEY_SERVICE_NAME, typeof(System.String));
            }
            catch { _strServiceName = null; }

            try
            {
                _strServiceAccountUsername = (string)configurationAppSettings.GetValue(KEY_SERVICE_ACCOUNT_USERNAME, typeof(System.String));
            }
            catch { _strServiceAccountUsername = null; }

            try
            {
                _strServiceAccountPassword = (string)configurationAppSettings.GetValue(KEY_SERVICE_ACCOUNT_PASSWORD, typeof(System.String));
            }
            catch { _strServiceAccountPassword = null; }
        }
    }
}
