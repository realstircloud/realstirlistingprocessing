﻿using System;
using System.Configuration;

namespace Realstir.Engine.Listing.Db.DAL
{
    public class DataBase
    {
        protected string connectionString = ConfigurationManager.AppSettings["ConnectionStringSQLite"];
        protected string dbPath = null;
        protected bool hasError = false;

        public DataBase()
        {
            //if (connectionString == null)
            //{
            //    String dbPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + System.IO.Path.DirectorySeparatorChar + "main.db";
            //    connectionString = string.Format("Data Source={0}; Version=3; FailIfMissing=True; Foreign Keys=True;", dbPath);
            //}
        }
        public bool HasError
        {
            get { return hasError; }
        }
        public string combineFieldList(string[] arFields)
        {
            string fieldsText = string.Empty;
            foreach(string s in arFields)
            {
                if (fieldsText.Length == 0)
                {
                    fieldsText += s;
                }
                else
                {
                    fieldsText += "," + s;
                }
            }
            return fieldsText;
        }
        public string generateUpdateStatement(string[] arFields, string tablename, string fieldid, long ID)
        {
            string statement = string.Empty;
            string fieldsText = string.Empty;
            fieldsText += string.Format("UPDATE {0} SET ", tablename);
            int cnt = 0;
            foreach (string s in arFields)
            {
                if (cnt == 0)
                {
                    fieldsText += string.Format("{0} = @{1}", s, s.ToUpper());
                }
                else
                {
                    fieldsText += string.Format(", {0} = @{1}", s, s.ToUpper());
                }
                cnt++;
            }
            statement += string.Format("{0} WHERE {1} = {2}", fieldsText, fieldid, ID);
            return statement;
        }
        public string generateInsertStatement(string[] arFields, string tablename)
        {
            string statement = string.Empty;
            string fieldsText1 = string.Empty;
            string fieldsText2 = string.Empty;
            int cnt = 0;
            foreach (string s in arFields)
            {
                if (cnt == 0)
                {
                    fieldsText1 += string.Format("{0}", s);
                }
                else
                {
                    fieldsText1 += string.Format(", {0}", s);
                }
                cnt++;
            }
            cnt = 0;
            foreach (string s in arFields)
            {
                if (cnt == 0)
                {
                    fieldsText2 += string.Format("@{0}", s.ToUpper());
                }
                else
                {
                    fieldsText2 += string.Format(", @{0}", s.ToUpper());
                }
                cnt++;
            }
            statement += string.Format("INSERT INTO {0} ({1}) VALUES ({2})", tablename, fieldsText1, fieldsText2);

            return statement;
        }
        public string generateDeleteStatement(string tablename, string fieldid, long ID)
        {
            string statement = string.Empty;
            string fieldsText = string.Empty;
            fieldsText += string.Format("DELETE FROM {0} ", tablename);           
            statement += string.Format("{0} WHERE {1} = {2}", fieldsText, fieldid, ID);
            return statement;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }


    }
}
