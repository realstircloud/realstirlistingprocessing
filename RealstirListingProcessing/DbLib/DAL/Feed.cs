﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
namespace Realstir.Engine.Listing.Db.DAL
{
    public class Feed : DataBase
    {
        private long _lFeedID = 0;
        private long _lFeedTypeID = 0;
        private long _lFeedStateID = 0;
        private DateTime _dtDateCreated = dtNull;
        private DateTime _dtDateModified = dtNull;
        private DateTime _dtDateProcessed = dtNull;
        private DateTime _dtDateReceived = dtNull;
        private DateTime _dtDateBeginProcessing = dtNull;
        private DateTime _dtDateEndProcessing = dtNull;
        private DateTime _dtDateDeflated = dtNull;
        private DateTime _dtDateClustered = dtNull;
        private string _strDatePublishedTextStr = null;
        private long _lRecordCount = 0;
        private bool? _bIsError = null;
        private string _strErrorStr = null;
        private bool? _bIsProcessing = null;
        private bool? _bIsProcessed = null;
        private string _strIncomingFilename = null;
        private string _strBlobRef = null;
        private string _strBlobRefUncompressed = null;
        private long _lIncomingContentLength = 0;
        private string _strIncomingContentType = null;
        private long _lProcessingState = 0;
        private string _strFeedXmlVersionstamp = null;
        private string _strFeedXmlVersion = null;
        private string _strFeedXmlKey = null;
        private string _strLocalDirName = null;
        private string _strLocalDirPath = null;
        private long _lProcessedRecordCount = 0;
        private long _lNumberOfClusters = 0;
        private DateTime _dtDateBeginDownload = dtNull;
        private DateTime _dtDateEndDownload = dtNull;
        private DateTime _dtDateBeginDeflate = dtNull;
        private DateTime _dtDateEndDeflate = dtNull;
        private DateTime _dtDateBeginClustering = dtNull;
        private DateTime _dtDateEndClustering = dtNull;
        private DateTime _dtDateBeginUploading = dtNull;
        private DateTime _dtDateEndUploading = dtNull;

        private static DateTime dtNull = new DateTime();


        public static readonly string FIELD_ID = "feed_id"; 
        public static readonly string FEED_TYPE_ID = "feed_type_id";
        public static readonly string FEED_STATE_ID = "feed_state_id"; 
        public static readonly string DATE_CREATED = "date_created"; 
        public static readonly string DATE_MODIFIED = "date_modified";
        public static readonly string DATE_PROCESSED = "date_processed";
        public static readonly string DATE_RECEIVED = "date_received";
        public static readonly string DATE_BEGIN_PROCESSING = "date_begin_processing";
        public static readonly string DATE_END_PROCESSING = "date_end_processing";
        public static readonly string DATE_DEFLATED = "date_deflated";
        public static readonly string DATE_CLUSTERED = "date_clustered";
        public static readonly string DATE_PUBLISHED_TEXT_STR = "date_published_text_str";
        public static readonly string RECORD_COUNT = "record_count";
        public static readonly string IS_ERROR = "is_error";
        public static readonly string ERROR_STR = "error_str";
        public static readonly string IS_PROCESSING = "is_processing";
        public static readonly string IS_PROCESSED = "is_processed";
        public static readonly string INCOMING_FILENAME = "incoming_filename";
        public static readonly string BLOB_REF = "blob_ref";
        public static readonly string BLOB_REF_UNCOMPRESSED = "blob_ref_uncompressed";
        public static readonly string INCOMING_CONTENT_LENGTH = "incoming_content_length";
        public static readonly string INCOMING_CONTENT_TYPE = "incoming_content_type";
        public static readonly string PROCESSING_STATE = "processing_state";
        public static readonly string FEED_XML_VERSIONSTAMP = "feed_xml_versionstamp";
        public static readonly string FEED_XML_VERSION = "feed_xml_version";
        public static readonly string FEED_XML_KEY = "feed_xml_key";
        public static readonly string LOCAL_DIR_NAME = "local_dir_name";
        public static readonly string LOCAL_DIR_PATH = "local_dir_path";
        public static readonly string PROCESSED_RECORD_COUNT = "processed_record_count";
        public static readonly string NUMBER_OF_CLUSTERS = "number_of_clusters";
        public static readonly string DATE_BEGIN_DOWNLOAD = "date_begin_download";
        public static readonly string DATE_END_DOWNLOAD = "date_end_download";
        public static readonly string DATE_BEGIN_DEFLATE = "date_begin_deflate";
        public static readonly string DATE_END_DEFLATE = "date_end_deflate";
        public static readonly string DATE_BEGIN_CLUSTERING = "date_begin_clustering";
        public static readonly string DATE_END_CLUSTERING = "date_end_clustering";
        public static readonly string DATE_BEGIN_UPLOADING = "date_begin_uploading";
        public static readonly string DATE_END_UPLOADING = "date_end_uploading";

        private string[] fieldsAll = new string[] {  "feed_id","feed_type_id","feed_state_id","date_created","date_modified","date_processed","date_received","date_begin_processing","date_end_processing","date_deflated","date_clustered"
,"date_published_text_str","record_count","is_error","error_str","is_processing","is_processed","incoming_filename","blob_ref","blob_ref_uncompressed","incoming_content_length"
,"incoming_content_type","processing_state","feed_xml_versionstamp","feed_xml_version","feed_xml_key","local_dir_name","local_dir_path","processed_record_count","number_of_clusters"
,"date_begin_download","date_end_download","date_begin_deflate","date_end_deflate","date_begin_clustering","date_end_clustering","date_begin_uploading","date_end_uploading" };

        private string[] fieldsUpdate = new string[] { "feed_type_id","feed_state_id","date_modified","date_processed","date_received","date_begin_processing","date_end_processing","date_deflated","date_clustered"
,"date_published_text_str","record_count","is_error","error_str","is_processing","is_processed","incoming_filename","blob_ref","blob_ref_uncompressed","incoming_content_length"
,"incoming_content_type","processing_state","feed_xml_versionstamp","feed_xml_version","feed_xml_key","local_dir_name","local_dir_path","processed_record_count","number_of_clusters"
,"date_begin_download","date_end_download","date_begin_deflate","date_end_deflate","date_begin_clustering","date_end_clustering","date_begin_uploading","date_end_uploading" };

        private string[] fieldsInsert = new string[] { "feed_type_id","feed_state_id","date_processed","date_received","date_begin_processing","date_end_processing","date_deflated","date_clustered"
,"date_published_text_str","record_count","is_error","error_str","is_processing","is_processed","incoming_filename","blob_ref","blob_ref_uncompressed","incoming_content_length"
,"incoming_content_type","processing_state","feed_xml_versionstamp","feed_xml_version","feed_xml_key","local_dir_name","local_dir_path","processed_record_count","number_of_clusters"
,"date_begin_download","date_end_download","date_begin_deflate","date_end_deflate","date_begin_clustering","date_end_clustering","date_begin_uploading","date_end_uploading" };


        public static readonly string TABLE_NAME = "feed";

        //properties
        /// <summary>FeedID is a Property in the Feed Class of type long</summary>
        public long FeedID
        {
            get { return _lFeedID; }
            set { _lFeedID = value; }
        }
        /// <summary>FeedTypeID is a Property in the Feed Class of type long</summary>
        public long FeedTypeID
        {
            get { return _lFeedTypeID; }
            set { _lFeedTypeID = value; }
        }
        /// <summary>FeedStateID is a Property in the Feed Class of type long</summary>
        public long FeedStateID
        {
            get { return _lFeedStateID; }
            set { _lFeedStateID = value; }
        }
        /// <summary>DateCreated is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>DateModified is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateModified
        {
            get { return _dtDateModified; }
            set { _dtDateModified = value; }
        }
        /// <summary>DateProcessed is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateProcessed
        {
            get { return _dtDateProcessed; }
            set { _dtDateProcessed = value; }
        }
        /// <summary>DateReceived is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateReceived
        {
            get { return _dtDateReceived; }
            set { _dtDateReceived = value; }
        }
        /// <summary>DateBeginProcessing is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateBeginProcessing
        {
            get { return _dtDateBeginProcessing; }
            set { _dtDateBeginProcessing = value; }
        }
        /// <summary>DateEndProcessing is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateEndProcessing
        {
            get { return _dtDateEndProcessing; }
            set { _dtDateEndProcessing = value; }
        }
        /// <summary>DateDeflated is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateDeflated
        {
            get { return _dtDateDeflated; }
            set { _dtDateDeflated = value; }
        }
        /// <summary>DateClustered is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateClustered
        {
            get { return _dtDateClustered; }
            set { _dtDateClustered = value; }
        }
        /// <summary>DatePublishedTextStr is a Property in the Feed Class of type String</summary>
        public string DatePublishedTextStr
        {
            get { return _strDatePublishedTextStr; }
            set { _strDatePublishedTextStr = value; }
        }
        /// <summary>RecordCount is a Property in the Feed Class of type long</summary>
        public long RecordCount
        {
            get { return _lRecordCount; }
            set { _lRecordCount = value; }
        }
        /// <summary>IsError is a Property in the Feed Class of type bool</summary>
        public bool? IsError
        {
            get { return _bIsError; }
            set { _bIsError = value; }
        }
        /// <summary>ErrorStr is a Property in the Feed Class of type String</summary>
        public string ErrorStr
        {
            get { return _strErrorStr; }
            set { _strErrorStr = value; }
        }
        /// <summary>IsProcessing is a Property in the Feed Class of type bool</summary>
        public bool? IsProcessing
        {
            get { return _bIsProcessing; }
            set { _bIsProcessing = value; }
        }
        /// <summary>IsProcessed is a Property in the Feed Class of type bool</summary>
        public bool? IsProcessed
        {
            get { return _bIsProcessed; }
            set { _bIsProcessed = value; }
        }
        /// <summary>IncomingFilename is a Property in the Feed Class of type String</summary>
        public string IncomingFilename
        {
            get { return _strIncomingFilename; }
            set { _strIncomingFilename = value; }
        }
        /// <summary>BlobRef is a Property in the Feed Class of type String</summary>
        public string BlobRef
        {
            get { return _strBlobRef; }
            set { _strBlobRef = value; }
        }
        /// <summary>BlobRefUncompressed is a Property in the Feed Class of type String</summary>
        public string BlobRefUncompressed
        {
            get { return _strBlobRefUncompressed; }
            set { _strBlobRefUncompressed = value; }
        }
        /// <summary>IncomingContentLength is a Property in the Feed Class of type long</summary>
        public long IncomingContentLength
        {
            get { return _lIncomingContentLength; }
            set { _lIncomingContentLength = value; }
        }
        /// <summary>IncomingContentType is a Property in the Feed Class of type String</summary>
        public string IncomingContentType
        {
            get { return _strIncomingContentType; }
            set { _strIncomingContentType = value; }
        }
        /// <summary>ProcessingState is a Property in the Feed Class of type long</summary>
        public long ProcessingState
        {
            get { return _lProcessingState; }
            set { _lProcessingState = value; }
        }
        /// <summary>FeedXmlVersionstamp is a Property in the Feed Class of type String</summary>
        public string FeedXmlVersionstamp
        {
            get { return _strFeedXmlVersionstamp; }
            set { _strFeedXmlVersionstamp = value; }
        }
        /// <summary>FeedXmlVersion is a Property in the Feed Class of type String</summary>
        public string FeedXmlVersion
        {
            get { return _strFeedXmlVersion; }
            set { _strFeedXmlVersion = value; }
        }
        /// <summary>FeedXmlKey is a Property in the Feed Class of type String</summary>
        public string FeedXmlKey
        {
            get { return _strFeedXmlKey; }
            set { _strFeedXmlKey = value; }
        }
        /// <summary>LocalDirName is a Property in the Feed Class of type String</summary>
        public string LocalDirName
        {
            get { return _strLocalDirName; }
            set { _strLocalDirName = value; }
        }
        /// <summary>LocalDirPath is a Property in the Feed Class of type String</summary>
        public string LocalDirPath
        {
            get { return _strLocalDirPath; }
            set { _strLocalDirPath = value; }
        }
        /// <summary>ProcessedRecordCount is a Property in the Feed Class of type long</summary>
        public long ProcessedRecordCount
        {
            get { return _lProcessedRecordCount; }
            set { _lProcessedRecordCount = value; }
        }
        /// <summary>NumberOfClusters is a Property in the Feed Class of type long</summary>
        public long NumberOfClusters
        {
            get { return _lNumberOfClusters; }
            set { _lNumberOfClusters = value; }
        }
        /// <summary>DateBeginDownload is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateBeginDownload
        {
            get { return _dtDateBeginDownload; }
            set { _dtDateBeginDownload = value; }
        }
        /// <summary>DateEndDownload is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateEndDownload
        {
            get { return _dtDateEndDownload; }
            set { _dtDateEndDownload = value; }
        }
        /// <summary>DateBeginDeflate is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateBeginDeflate
        {
            get { return _dtDateBeginDeflate; }
            set { _dtDateBeginDeflate = value; }
        }
        /// <summary>DateEndDeflate is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateEndDeflate
        {
            get { return _dtDateEndDeflate; }
            set { _dtDateEndDeflate = value; }
        }
        /// <summary>DateBeginClustering is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateBeginClustering
        {
            get { return _dtDateBeginClustering; }
            set { _dtDateBeginClustering = value; }
        }
        /// <summary>DateEndClustering is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateEndClustering
        {
            get { return _dtDateEndClustering; }
            set { _dtDateEndClustering = value; }
        }
        /// <summary>DateBeginUploading is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateBeginUploading
        {
            get { return _dtDateBeginUploading; }
            set { _dtDateBeginUploading = value; }
        }
        /// <summary>DateEndUploading is a Property in the Feed Class of type DateTime</summary>
        public DateTime DateEndUploading
        {
            get { return _dtDateEndUploading; }
            set { _dtDateEndUploading = value; }
        }


        public Feed()
        {
        }
        public Feed(long l)
        {
            FeedID = l;
            try
            {
                sqlLoad();
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public Feed(SQLiteDataReader rd)
        {
            sqlParseResultSet(rd);
        }
        public void Load()
        {
            try
            {
                sqlLoad();
            }
            catch (Exception e)
            {
                hasError = true;
            }

        }
        public List<Feed> Load(string whereClause)
        {
            List<Feed> lstReturn = null;
            try
            {
                lstReturn = sqlLoad(whereClause);
            }
            catch (Exception e)
            {
                hasError = true;
            }
            return lstReturn;

        }
        public void Update()
        {
            bool bExist = false;
            try
            {
                bExist = Exist();
                if (bExist)
                {
                    sqlUpdate();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public void Save()
        {
            try
            {
                bool bExist = false;

                bExist = Exist();
                if (!bExist)
                {
                    sqlInsert();
                }
                else
                {
                    sqlUpdate();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public void Delete()
        {
            try
            {
                if (FeedID > 0)
                {
                    sqlDelete();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public bool Exist()
        {
            bool bReturn = false;
            try
            {
                bReturn = sqlExist();
            }
            catch (Exception e)
            {
                hasError = true;
            }

            return bReturn;
        }
        protected void sqlInsert()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateInsertStatement(fieldsInsert, TABLE_NAME);
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@FEED_TYPE_ID", FeedTypeID);
                    cmd.Parameters.AddWithValue("@FEED_TYPE_ID", FeedTypeID);
                    cmd.Parameters.AddWithValue("@FEED_STATE_ID", FeedStateID);
                    cmd.Parameters.AddWithValue("@DATE_PROCESSED", DateProcessed);
                    cmd.Parameters.AddWithValue("@DATE_RECEIVED", DateReceived);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_PROCESSING", DateBeginProcessing);
                    cmd.Parameters.AddWithValue("@DATE_END_PROCESSING", DateEndProcessing);
                    cmd.Parameters.AddWithValue("@DATE_DEFLATED", DateDeflated);
                    cmd.Parameters.AddWithValue("@DATE_CLUSTERED", DateClustered);
                    cmd.Parameters.AddWithValue("@DATE_PUBLISHED_TEXT_STR", DatePublishedTextStr);
                    cmd.Parameters.AddWithValue("@RECORD_COUNT", RecordCount);
                    cmd.Parameters.AddWithValue("@IS_ERROR", IsError);
                    cmd.Parameters.AddWithValue("@ERROR_STR", ErrorStr);
                    cmd.Parameters.AddWithValue("@IS_PROCESSING", IsProcessing);
                    cmd.Parameters.AddWithValue("@IS_PROCESSED", IsProcessed);
                    cmd.Parameters.AddWithValue("@INCOMING_FILENAME", IncomingFilename);
                    cmd.Parameters.AddWithValue("@BLOB_REF", BlobRef);
                    cmd.Parameters.AddWithValue("@BLOB_REF_UNCOMPRESSED", BlobRefUncompressed);
                    cmd.Parameters.AddWithValue("@INCOMING_CONTENT_LENGTH", IncomingContentLength);
                    cmd.Parameters.AddWithValue("@INCOMING_CONTENT_TYPE", IncomingContentType);
                    cmd.Parameters.AddWithValue("@PROCESSING_STATE", ProcessingState);
                    cmd.Parameters.AddWithValue("@FEED_XML_VERSIONSTAMP", FeedXmlVersionstamp);
                    cmd.Parameters.AddWithValue("@FEED_XML_VERSION", FeedXmlVersion);
                    cmd.Parameters.AddWithValue("@FEED_XML_KEY", FeedXmlKey);
                    cmd.Parameters.AddWithValue("@LOCAL_DIR_NAME", LocalDirName);
                    cmd.Parameters.AddWithValue("@LOCAL_DIR_PATH", LocalDirPath);
                    cmd.Parameters.AddWithValue("@PROCESSED_RECORD_COUNT", ProcessedRecordCount);
                    cmd.Parameters.AddWithValue("@NUMBER_OF_CLUSTERS", NumberOfClusters);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_DOWNLOAD", DateBeginDownload);
                    cmd.Parameters.AddWithValue("@DATE_END_DOWNLOAD", DateEndDownload);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_DEFLATE", DateBeginDeflate);
                    cmd.Parameters.AddWithValue("@DATE_END_DEFLATE", DateEndDeflate);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_CLUSTERING", DateBeginClustering);
                    cmd.Parameters.AddWithValue("@DATE_END_CLUSTERING", DateEndClustering);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_UPLOADING", DateBeginUploading);
                    cmd.Parameters.AddWithValue("@DATE_END_UPLOADING", DateEndUploading);

                    result = cmd.ExecuteNonQuery();
                    FeedID = conn.LastInsertRowId;

                }
                conn.Close();
            }
        }
        protected bool sqlExist()
        {
            bool bExist = false;
            string SELECT_SQL = string.Format("SELECT {0} FROM {1} WHERE {2} = {3} LIMIT 1", FIELD_ID, TABLE_NAME, FIELD_ID, FeedID);
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(SELECT_SQL, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        bExist = rdr.HasRows;
                    }
                }
                conn.Close();
            }

            return bExist;
        }
        protected void sqlUpdate()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateUpdateStatement(fieldsUpdate, TABLE_NAME, FIELD_ID, FeedID);
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@DATE_MODIFIED", DateTime.Now);
                    cmd.Parameters.AddWithValue("@FEED_TYPE_ID", FeedTypeID);
                    cmd.Parameters.AddWithValue("@FEED_TYPE_ID", FeedTypeID);
                    cmd.Parameters.AddWithValue("@FEED_STATE_ID", FeedStateID);
                    cmd.Parameters.AddWithValue("@DATE_PROCESSED", DateProcessed);
                    cmd.Parameters.AddWithValue("@DATE_RECEIVED", DateReceived);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_PROCESSING", DateBeginProcessing);
                    cmd.Parameters.AddWithValue("@DATE_END_PROCESSING", DateEndProcessing);
                    cmd.Parameters.AddWithValue("@DATE_DEFLATED", DateDeflated);
                    cmd.Parameters.AddWithValue("@DATE_CLUSTERED", DateClustered);
                    cmd.Parameters.AddWithValue("@DATE_PUBLISHED_TEXT_STR", DatePublishedTextStr);
                    cmd.Parameters.AddWithValue("@RECORD_COUNT", RecordCount);
                    cmd.Parameters.AddWithValue("@IS_ERROR", IsError);
                    cmd.Parameters.AddWithValue("@ERROR_STR", ErrorStr);
                    cmd.Parameters.AddWithValue("@IS_PROCESSING", IsProcessing);
                    cmd.Parameters.AddWithValue("@IS_PROCESSED", IsProcessed);
                    cmd.Parameters.AddWithValue("@INCOMING_FILENAME", IncomingFilename);
                    cmd.Parameters.AddWithValue("@BLOB_REF", BlobRef);
                    cmd.Parameters.AddWithValue("@BLOB_REF_UNCOMPRESSED", BlobRefUncompressed);
                    cmd.Parameters.AddWithValue("@INCOMING_CONTENT_LENGTH", IncomingContentLength);
                    cmd.Parameters.AddWithValue("@INCOMING_CONTENT_TYPE", IncomingContentType);
                    cmd.Parameters.AddWithValue("@PROCESSING_STATE", ProcessingState);
                    cmd.Parameters.AddWithValue("@FEED_XML_VERSIONSTAMP", FeedXmlVersionstamp);
                    cmd.Parameters.AddWithValue("@FEED_XML_VERSION", FeedXmlVersion);
                    cmd.Parameters.AddWithValue("@FEED_XML_KEY", FeedXmlKey);
                    cmd.Parameters.AddWithValue("@LOCAL_DIR_NAME", LocalDirName);
                    cmd.Parameters.AddWithValue("@LOCAL_DIR_PATH", LocalDirPath);
                    cmd.Parameters.AddWithValue("@PROCESSED_RECORD_COUNT", ProcessedRecordCount);
                    cmd.Parameters.AddWithValue("@NUMBER_OF_CLUSTERS", NumberOfClusters);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_DOWNLOAD", DateBeginDownload);
                    cmd.Parameters.AddWithValue("@DATE_END_DOWNLOAD", DateEndDownload);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_DEFLATE", DateBeginDeflate);
                    cmd.Parameters.AddWithValue("@DATE_END_DEFLATE", DateEndDeflate);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_CLUSTERING", DateBeginClustering);
                    cmd.Parameters.AddWithValue("@DATE_END_CLUSTERING", DateEndClustering);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_UPLOADING", DateBeginUploading);
                    cmd.Parameters.AddWithValue("@DATE_END_UPLOADING", DateEndUploading);

                    result = cmd.ExecuteNonQuery();

                }
                conn.Close();
            }

        }
        protected void sqlDelete()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateDeleteStatement(TABLE_NAME, FIELD_ID, FeedID);
                    result = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
        protected void sqlLoad()
        {
            string combinedFields = combineFieldList(fieldsAll);
            string SELECT_SQL = string.Format("SELECT {0} FROM {1}", combinedFields, TABLE_NAME);
            string sql = sql = SELECT_SQL;
            if (FeedID > 0)
            {
                sql = string.Format("{0} WHERE {1} = {2}", SELECT_SQL, FIELD_ID, FeedID);
            }

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            sqlParseResultSet(rdr);
                        }
                    }
                }
                conn.Close();
            }
        }
        protected List<Feed> sqlLoad(string whereClause)
        {
            List<Feed> lstReturn = new List<Feed>();

            string combinedFields = combineFieldList(fieldsAll);
            string SELECT_SQL = string.Format("SELECT {0} FROM {1}", combinedFields, TABLE_NAME);

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                string sql = sql = SELECT_SQL;
                sql += " " + whereClause;

                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        Feed o = null;
                        while (rdr.Read())
                        {
                            o = sqlParseResultSetObj(rdr);
                            lstReturn.Add(o);
                        }
                    }
                }
                conn.Close();
            }
            return lstReturn;
        }
        protected void sqlParseResultSet(SQLiteDataReader rdr)
        {
            this.FeedID = long.Parse(rdr[FIELD_ID].ToString());
            try
            {
                this.FeedTypeID = Convert.ToInt32(rdr[FEED_TYPE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.FeedStateID = Convert.ToInt32(rdr[FEED_STATE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.DateCreated = DateTime.Parse(rdr[DATE_CREATED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateModified = DateTime.Parse(rdr[DATE_MODIFIED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateProcessed = DateTime.Parse(rdr[DATE_PROCESSED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateReceived = DateTime.Parse(rdr[DATE_RECEIVED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateBeginProcessing = DateTime.Parse(rdr[DATE_BEGIN_PROCESSING].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateEndProcessing = DateTime.Parse(rdr[DATE_END_PROCESSING].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateDeflated = DateTime.Parse(rdr[DATE_DEFLATED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateClustered = DateTime.Parse(rdr[DATE_CLUSTERED].ToString());
            }
            catch
            {
            }
            this.DatePublishedTextStr = rdr[DATE_PUBLISHED_TEXT_STR].ToString().Trim();
            try
            {
                this.RecordCount = Convert.ToInt32(rdr[RECORD_COUNT].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsError = Convert.ToBoolean(rdr[IS_ERROR].ToString().Trim());
            }
            catch { }
            try
            {
                this.ErrorStr = rdr[ERROR_STR].ToString().Trim();
            }
            catch { }
            try
            {
                this.IsProcessing = Convert.ToBoolean(rdr[IS_PROCESSING].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsProcessed = Convert.ToBoolean(rdr[IS_PROCESSED].ToString().Trim());
            }
            catch { }
            this.IncomingFilename = rdr[INCOMING_FILENAME].ToString().Trim();
            this.BlobRef = rdr[BLOB_REF].ToString().Trim();
            this.BlobRefUncompressed = rdr[BLOB_REF_UNCOMPRESSED].ToString().Trim();
            try
            {
                this.IncomingContentLength = Convert.ToInt32(rdr[INCOMING_CONTENT_LENGTH].ToString().Trim());
            }
            catch { }
            this.IncomingContentType = rdr[INCOMING_CONTENT_TYPE].ToString().Trim();
            try
            {
                this.ProcessingState = Convert.ToInt32(rdr[PROCESSING_STATE].ToString().Trim());
            }
            catch { }
            this.FeedXmlVersionstamp = rdr[FEED_XML_VERSIONSTAMP].ToString().Trim();
            this.FeedXmlVersion = rdr[FEED_XML_VERSION].ToString().Trim();
            this.FeedXmlKey = rdr[FEED_XML_KEY].ToString().Trim();
            this.LocalDirName = rdr[LOCAL_DIR_NAME].ToString().Trim();
            this.LocalDirPath = rdr[LOCAL_DIR_PATH].ToString().Trim();
            try
            {
                this.ProcessedRecordCount = Convert.ToInt32(rdr[PROCESSED_RECORD_COUNT].ToString().Trim());
            }
            catch { }
            try
            {
                this.NumberOfClusters = Convert.ToInt32(rdr[NUMBER_OF_CLUSTERS].ToString().Trim());
            }
            catch { }
            try
            {
                this.DateBeginDownload = DateTime.Parse(rdr[DATE_BEGIN_DOWNLOAD].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateEndDownload = DateTime.Parse(rdr[DATE_END_DOWNLOAD].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateBeginDeflate = DateTime.Parse(rdr[DATE_BEGIN_DEFLATE].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateEndDeflate = DateTime.Parse(rdr[DATE_END_DEFLATE].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateBeginClustering = DateTime.Parse(rdr[DATE_BEGIN_CLUSTERING].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateEndClustering = DateTime.Parse(rdr[DATE_END_CLUSTERING].ToString());
            }
            catch
            {
            }

            try
            {
                this.DateBeginUploading = DateTime.Parse(rdr[DATE_BEGIN_UPLOADING].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateEndUploading = DateTime.Parse(rdr[DATE_END_UPLOADING].ToString());
            }
            catch
            {
            }

        }
        protected Feed sqlParseResultSetObj(SQLiteDataReader rdr)
        {
            Feed o = new Feed();
            o.FeedID = long.Parse(rdr[FIELD_ID].ToString());
            try
            {
                o.FeedTypeID = Convert.ToInt32(rdr[FEED_TYPE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                o.FeedStateID = Convert.ToInt32(rdr[FEED_STATE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                o.DateCreated = DateTime.Parse(rdr[DATE_CREATED].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateModified = DateTime.Parse(rdr[DATE_MODIFIED].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateProcessed = DateTime.Parse(rdr[DATE_PROCESSED].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateReceived = DateTime.Parse(rdr[DATE_RECEIVED].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateBeginProcessing = DateTime.Parse(rdr[DATE_BEGIN_PROCESSING].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateEndProcessing = DateTime.Parse(rdr[DATE_END_PROCESSING].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateDeflated = DateTime.Parse(rdr[DATE_DEFLATED].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateClustered = DateTime.Parse(rdr[DATE_CLUSTERED].ToString());
            }
            catch
            {
            }
            o.DatePublishedTextStr = rdr[DATE_PUBLISHED_TEXT_STR].ToString().Trim();
            try
            {
                o.RecordCount = Convert.ToInt32(rdr[RECORD_COUNT].ToString().Trim());
            }
            catch { }
            try
            {
                o.IsError = Convert.ToBoolean(rdr[IS_ERROR].ToString().Trim());
            }
            catch { }
            try
            {
                o.ErrorStr = rdr[ERROR_STR].ToString().Trim();
            }
            catch { }
            try
            {
                o.IsProcessing = Convert.ToBoolean(rdr[IS_PROCESSING].ToString().Trim());
            }
            catch { }
            try
            {
                o.IsProcessed = Convert.ToBoolean(rdr[IS_PROCESSED].ToString().Trim());
            }
            catch { }
            o.IncomingFilename = rdr[INCOMING_FILENAME].ToString().Trim();
            o.BlobRef = rdr[BLOB_REF].ToString().Trim();
            o.BlobRefUncompressed = rdr[BLOB_REF_UNCOMPRESSED].ToString().Trim();
            try
            {
                o.IncomingContentLength = Convert.ToInt32(rdr[INCOMING_CONTENT_LENGTH].ToString().Trim());
            }
            catch { }
            o.IncomingContentType = rdr[INCOMING_CONTENT_TYPE].ToString().Trim();
            try
            {
                o.ProcessingState = Convert.ToInt32(rdr[PROCESSING_STATE].ToString().Trim());
            }
            catch { }
            o.FeedXmlVersionstamp = rdr[FEED_XML_VERSIONSTAMP].ToString().Trim();
            o.FeedXmlVersion = rdr[FEED_XML_VERSION].ToString().Trim();
            o.FeedXmlKey = rdr[FEED_XML_KEY].ToString().Trim();
            o.LocalDirName = rdr[LOCAL_DIR_NAME].ToString().Trim();
            o.LocalDirPath = rdr[LOCAL_DIR_PATH].ToString().Trim();
            try
            {
                o.ProcessedRecordCount = Convert.ToInt32(rdr[PROCESSED_RECORD_COUNT].ToString().Trim());
            }
            catch { }
            try
            {
                o.NumberOfClusters = Convert.ToInt32(rdr[NUMBER_OF_CLUSTERS].ToString().Trim());
            }
            catch { }
            try
            {
                o.DateBeginDownload = DateTime.Parse(rdr[DATE_BEGIN_DOWNLOAD].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateEndDownload = DateTime.Parse(rdr[DATE_END_DOWNLOAD].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateBeginDeflate = DateTime.Parse(rdr[DATE_BEGIN_DEFLATE].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateEndDeflate = DateTime.Parse(rdr[DATE_END_DEFLATE].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateBeginClustering = DateTime.Parse(rdr[DATE_BEGIN_CLUSTERING].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateEndClustering = DateTime.Parse(rdr[DATE_END_CLUSTERING].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateBeginUploading = DateTime.Parse(rdr[DATE_BEGIN_UPLOADING].ToString());
            }
            catch
            {
            }
            try
            {
                o.DateEndUploading = DateTime.Parse(rdr[DATE_END_UPLOADING].ToString());
            }
            catch
            {
            }
            return o;
        }
    }
}
