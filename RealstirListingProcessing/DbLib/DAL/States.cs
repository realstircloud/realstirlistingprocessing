﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace Realstir.Engine.Listing.Db.DAL
{
    public class States : DataBase
    {
        public long StatesID { get; set; }
        public DateTime DateCreated { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string VisibleCode { get; set; }

        public static readonly string FIELD_ID = "states_id";
        public static readonly string FIELD_DATE_CREATED = "date_created";
        public static readonly string FIELD_CODE = "code";
        public static readonly string FIELD_DESCRIPTION = "description";
        public static readonly string FIELD_VISIBLE_CODE = "visible_code";

        private string[] fieldsAll = new string[] { "states_id", "date_created", "code", "description", "visible_code" };
        private string[] fieldsUpdate = new string[] { "date_modified", "code", "description", "visible_code" };
        private string[] fieldsInsert = new string[] { "states_id", "code", "description", "visible_code" };
        public static readonly string TABLE_NAME = "states";

        public States()
        {
        }
        public States(long l)
        {
            StatesID = l;
            try
            {
                sqlLoad();
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public States(SQLiteDataReader rd)
        {
            sqlParseResultSet(rd);
        }
        public void Load()
        {
            try
            {
                sqlLoad();
            }
            catch (Exception e)
            {
                hasError = true;
            }

        }
        public List<States> Load(string whereClause)
        {
            List<States> lstReturn = null;
            try
            {
                lstReturn = sqlLoad(whereClause);
            }
            catch (Exception e)
            {
                hasError = true;
            }
            return lstReturn;

        }
        public void Update()
        {
            bool bExist = false;
            try
            {
                bExist = Exist();
                if (bExist)
                {
                    sqlUpdate();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public void Save()
        {
            try
            {
                bool bExist = false;

                bExist = Exist();
                if (!bExist)
                {
                    sqlInsert();
                }
                else
                {
                    sqlUpdate();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public void Delete()
        {
            try
            {
                if (StatesID > 0)
                {
                    sqlDelete();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public bool Exist()
        {
            bool bReturn = false;
            try
            {
                bReturn = sqlExist();
            }
            catch (Exception e)
            {
                hasError = true;
            }

            return bReturn;
        }
        protected void sqlInsert()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateInsertStatement(fieldsInsert, TABLE_NAME);
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@STATES_ID", StatesID);
                    cmd.Parameters.AddWithValue("@CODE", Code);
                    cmd.Parameters.AddWithValue("@DESCRIPTION", Description);
                    cmd.Parameters.AddWithValue("@VISIBLE_CODE", VisibleCode);
                    result = cmd.ExecuteNonQuery();

                }
                conn.Close();
            }
        }
        protected bool sqlExist()
        {
            bool bExist = false;
            string SELECT_SQL = string.Format("SELECT {0} FROM {1} WHERE {2} = {3} LIMIT 1", FIELD_ID, TABLE_NAME, FIELD_ID, StatesID);
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(SELECT_SQL, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        bExist = rdr.HasRows;
                    }
                }
                conn.Close();
            }

            return bExist;
        }
        protected void sqlUpdate()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateUpdateStatement(fieldsUpdate, TABLE_NAME, FIELD_ID, StatesID);
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@DATE_MODIFIED", DateTime.Now);
                    cmd.Parameters.AddWithValue("@CODE", Code);
                    cmd.Parameters.AddWithValue("@DESCRIPTION", Description);
                    cmd.Parameters.AddWithValue("@VISIBLE_CODE", VisibleCode);
                    result = cmd.ExecuteNonQuery();

                }
                conn.Close();
            }

        }
        protected void sqlDelete()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateDeleteStatement(TABLE_NAME, FIELD_ID, StatesID);
                    result = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
        protected void sqlLoad()
        {
            string combinedFields = combineFieldList(fieldsAll);
            string SELECT_SQL = string.Format("SELECT {0} FROM {1}", combinedFields, TABLE_NAME);
            string sql = sql = SELECT_SQL;
            if (StatesID > 0)
            {
                sql = string.Format("{0} WHERE {1} = {2}", SELECT_SQL, FIELD_ID, StatesID);
            }

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            sqlParseResultSet(rdr);
                        }
                    }
                }
                conn.Close();
            }
        }
        protected List<States> sqlLoad(string whereClause)
        {
            List<States> lstReturn = new List<States>();

            string combinedFields = combineFieldList(fieldsAll);
            string SELECT_SQL = string.Format("SELECT {0} FROM {1}", combinedFields, TABLE_NAME);

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                string sql = sql = SELECT_SQL;
                sql += " " + whereClause;

                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        States o = null;
                        while (rdr.Read())
                        {
                            o = sqlParseResultSetObj(rdr);
                            lstReturn.Add(o);
                        }
                    }
                }
                conn.Close();
            }
            return lstReturn;
        }
        protected void sqlParseResultSet(SQLiteDataReader rdr)
        {
            this.StatesID = long.Parse(rdr[FIELD_ID].ToString());
            try
            {
                this.DateCreated = DateTime.Parse(rdr[FIELD_DATE_CREATED].ToString());
            }
            catch
            {
            }
            this.Code = rdr[FIELD_CODE].ToString().Trim();
            this.Description = rdr[FIELD_DESCRIPTION].ToString().Trim();
            this.VisibleCode = rdr[FIELD_VISIBLE_CODE].ToString().Trim();
        }
        protected States sqlParseResultSetObj(SQLiteDataReader rdr)
        {
            States o = new States();
            o.StatesID = long.Parse(rdr[FIELD_ID].ToString());
            try
            {
                o.DateCreated = DateTime.Parse(rdr[FIELD_DATE_CREATED].ToString());
            }
            catch
            {
            }
            o.Code = rdr[FIELD_CODE].ToString().Trim();
            o.Description = rdr[FIELD_DESCRIPTION].ToString().Trim();
            o.VisibleCode = rdr[FIELD_VISIBLE_CODE].ToString().Trim();
            return o;
        }
    }
}
