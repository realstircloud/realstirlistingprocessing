﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
namespace Realstir.Engine.Listing.Db.DAL
{
    public class FeedData : DataBase
    {
        public long FeedDataID { get; set; }
        public long FeedID { get; set; }
        public long FeedDataTypeID { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int ClusterNum { get; set; }
        public string DataRetsSource { get; set; }
        public string DataXml { get; set; }
        public string DataJson { get; set; }
        public string ListingKey { get; set; }
        public string Modificationtimestamp { get; set; }
        public string State { get; set; }
        public string County { get; set; }
        public string City { get; set; }
        public string Mlsid { get; set; }
        public string Mlsname { get; set; }
        public string Mlsnumber { get; set; }
        public string ListingStatus { get; set; }
        public bool IsError { get; set; }
        public string ErrorStr { get; set; }
        public bool IsUploading { get; set; }
        public bool IsUploaded { get; set; }
        public DateTime DateBeginUploading { get; set; }
        public DateTime DateEndUploading { get; set; }
        public int TotalTimeToUploadInMs { get; set; }

        public static readonly string FIELD_ID = "feed_data_id";
        public static readonly string FEED_ID = "feed_id";
        public static readonly string FEED_DATA_TYPE_ID = "feed_data_type_id";
        public static readonly string DATE_CREATED = "date_created";
        public static readonly string DATE_MODIFIED = "date_modified";
        public static readonly string CLUSTER_NUM = "cluster_num";
        public static readonly string DATA_RETS_SOURCE = "data_rets_source";
        public static readonly string DATA_XML = "data_xml";
        public static readonly string DATA_JSON = "data_json";
        public static readonly string LISTING_KEY = "listing_key";
        public static readonly string MODIFICATIONTIMESTAMP = "modificationtimestamp";
        public static readonly string STATE = "state";
        public static readonly string COUNTY = "county";
        public static readonly string CITY = "city";
        public static readonly string MLSID = "mlsid";
        public static readonly string MLSNAME = "mlsname";
        public static readonly string MLSNUMBER = "mlsnumber";
        public static readonly string LISTING_STATUS = "listing_status";
        public static readonly string IS_ERROR = "is_error";
        public static readonly string ERROR_STR = "error_str";
        public static readonly string IS_UPLOADING = "is_uploading";
        public static readonly string IS_UPLOADED = "is_uploaded";
        public static readonly string DATE_BEGIN_UPLOADING = "date_begin_uploading";
        public static readonly string DATE_END_UPLOADING = "date_end_uploading";
        public static readonly string TOTAL_TIME_TO_UPLOAD_IN_MS = "total_time_to_upload_in_ms";

        private string[] fieldsAll = new string[] { "feed_data_id", "feed_id", "feed_data_type_id", "date_created", "date_modified", "cluster_num", "data_rets_source", "data_xml", "data_json", "listing_key", "modificationtimestamp", "state", "county", "city", "mlsid", "mlsname", "mlsnumber", "listing_status", "is_error", "error_str", "is_uploading", "is_uploaded", "date_begin_uploading", "date_end_uploading", "total_time_to_upload_in_ms" };
        private string[] fieldsUpdate = new string[] { "feed_id", "feed_data_type_id", "date_modified", "cluster_num", "data_rets_source", "data_xml", "data_json", "listing_key", "modificationtimestamp", "state", "county", "city", "mlsid", "mlsname", "mlsnumber", "listing_status", "is_error", "error_str", "is_uploading", "is_uploaded", "date_begin_uploading", "date_end_uploading", "total_time_to_upload_in_ms" };
        private string[] fieldsInsert = new string[] { "feed_id", "feed_data_type_id", "cluster_num", "data_rets_source", "data_xml", "data_json", "listing_key", "modificationtimestamp", "state", "county", "city", "mlsid", "mlsname", "mlsnumber", "listing_status", "is_error", "error_str", "is_uploading", "is_uploaded", "date_begin_uploading", "date_end_uploading", "total_time_to_upload_in_ms" };

        public static readonly string TABLE_NAME = "feed_data";

        public FeedData()
        {
        }
        public FeedData(long l)
        {
            FeedDataID = l;
            try
            {
                sqlLoad();
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public FeedData(SQLiteDataReader rd)
        {
            sqlParseResultSet(rd);
        }
        public void Load()
        {
            try
            {
                sqlLoad();
            }
            catch (Exception e)
            {
                hasError = true;
            }

        }
        public List<FeedData> Load(string whereClause)
        {
            List<FeedData> lstReturn = null;
            try
            {
                lstReturn = sqlLoad(whereClause);
            }
            catch (Exception e)
            {
                hasError = true;
            }
            return lstReturn;

        }
        public void Update()
        {
            bool bExist = false;
            try
            {
                bExist = Exist();
                if (bExist)
                {
                    sqlUpdate();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public void Save()
        {
            try
            {
                bool bExist = false;

                bExist = Exist();
                if (!bExist)
                {
                    sqlInsert();
                }
                else
                {
                    sqlUpdate();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public void Delete()
        {
            try
            {
                if (FeedDataID > 0)
                {
                    sqlDelete();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
        }
        public int MaxClusterNumber()
        {
            int max = 0;
            try
            {
                if (FeedID > 0)
                {
                    max = sqlMaxClusterNumber();
                }
            }
            catch (Exception e)
            {
                hasError = true;
            }
            return max;
        }
        public bool Exist()
        {
            bool bReturn = false;
            try
            {
                bReturn = sqlExist();
            }
            catch (Exception e)
            {
                hasError = true;
            }

            return bReturn;
        }
        protected void sqlInsert()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateInsertStatement(fieldsInsert, TABLE_NAME);
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@FEED_ID", FeedID);
                    cmd.Parameters.AddWithValue("@FEED_DATA_TYPE_ID", FeedDataTypeID);
                    cmd.Parameters.AddWithValue("@CLUSTER_NUM", ClusterNum);
                    cmd.Parameters.AddWithValue("@DATA_RETS_SOURCE", DataRetsSource);
                    cmd.Parameters.AddWithValue("@DATA_XML", DataXml);
                    cmd.Parameters.AddWithValue("@DATA_JSON", DataJson);
                    cmd.Parameters.AddWithValue("@LISTING_KEY", ListingKey);
                    cmd.Parameters.AddWithValue("@MODIFICATIONTIMESTAMP", Modificationtimestamp);
                    cmd.Parameters.AddWithValue("@STATE", State);
                    cmd.Parameters.AddWithValue("@COUNTY", County);
                    cmd.Parameters.AddWithValue("@CITY", City);
                    cmd.Parameters.AddWithValue("@MLSID", Mlsid);
                    cmd.Parameters.AddWithValue("@MLSNAME", Mlsname);
                    cmd.Parameters.AddWithValue("@MLSNUMBER", Mlsnumber);
                    cmd.Parameters.AddWithValue("@LISTING_STATUS", ListingStatus);
                    cmd.Parameters.AddWithValue("@IS_ERROR", IsError);
                    cmd.Parameters.AddWithValue("@ERROR_STR", ErrorStr);
                    cmd.Parameters.AddWithValue("@IS_UPLOADING", IsUploading);
                    cmd.Parameters.AddWithValue("@IS_UPLOADED", IsUploaded);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_UPLOADING", DateBeginUploading);
                    cmd.Parameters.AddWithValue("@DATE_END_UPLOADING", DateEndUploading);
                    cmd.Parameters.AddWithValue("@TOTAL_TIME_TO_UPLOAD_IN_MS", TotalTimeToUploadInMs);

                    result = cmd.ExecuteNonQuery();
                    FeedDataID = conn.LastInsertRowId;

                }
                conn.Close();
            }
        }
        protected bool sqlExist()
        {
            bool bExist = false;
            string SELECT_SQL = string.Format("SELECT {0} FROM {1} WHERE {2} = {3} LIMIT 1", FIELD_ID, TABLE_NAME, FIELD_ID, FeedDataID);
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(SELECT_SQL, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        bExist = rdr.HasRows;
                    }
                }
                conn.Close();
            }

            return bExist;
        }
        protected void sqlUpdate()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateUpdateStatement(fieldsUpdate, TABLE_NAME, FIELD_ID, FeedDataID);
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@DATE_MODIFIED", DateTime.Now);
                    cmd.Parameters.AddWithValue("@FEED_ID", FeedID);
                    cmd.Parameters.AddWithValue("@FEED_DATA_TYPE_ID", FeedDataTypeID);
                    cmd.Parameters.AddWithValue("@CLUSTER_NUM", ClusterNum);
                    cmd.Parameters.AddWithValue("@DATA_RETS_SOURCE", DataRetsSource);
                    cmd.Parameters.AddWithValue("@DATA_XML", DataXml);
                    cmd.Parameters.AddWithValue("@DATA_JSON", DataXml);
                    cmd.Parameters.AddWithValue("@LISTING_KEY", ListingKey);
                    cmd.Parameters.AddWithValue("@MODIFICATIONTIMESTAMP", Modificationtimestamp);
                    cmd.Parameters.AddWithValue("@STATE", State);
                    cmd.Parameters.AddWithValue("@COUNTY", County);
                    cmd.Parameters.AddWithValue("@CITY", City);
                    cmd.Parameters.AddWithValue("@MLSID", Mlsid);
                    cmd.Parameters.AddWithValue("@MLSNAME", Mlsname);
                    cmd.Parameters.AddWithValue("@MLSNUMBER", Mlsnumber);
                    cmd.Parameters.AddWithValue("@LISTING_STATUS", ListingStatus);
                    cmd.Parameters.AddWithValue("@IS_ERROR", IsError);
                    cmd.Parameters.AddWithValue("@ERROR_STR", ErrorStr);
                    cmd.Parameters.AddWithValue("@IS_UPLOADING", IsUploading);
                    cmd.Parameters.AddWithValue("@IS_UPLOADED", IsUploaded);
                    cmd.Parameters.AddWithValue("@DATE_BEGIN_UPLOADING", DateBeginUploading);
                    cmd.Parameters.AddWithValue("@DATE_END_UPLOADING", DateEndUploading);
                    cmd.Parameters.AddWithValue("@TOTAL_TIME_TO_UPLOAD_IN_MS", TotalTimeToUploadInMs);

                    result = cmd.ExecuteNonQuery();

                }
                conn.Close();
            }

        }
        protected void sqlDelete()
        {
            int result = -1;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = generateDeleteStatement(TABLE_NAME, FIELD_ID, FeedDataID);
                    result = cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
        protected void sqlLoad()
        {
            string combinedFields = combineFieldList(fieldsAll);
            string SELECT_SQL = string.Format("SELECT {0} FROM {1}", combinedFields, TABLE_NAME);
            string sql = sql = SELECT_SQL;
            if (FeedDataID > 0)
            {
                sql = string.Format("{0} WHERE {1} = {2}", SELECT_SQL, FIELD_ID, FeedDataID);
            }

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        if (rdr.Read())
                        {
                            sqlParseResultSet(rdr);
                        }
                    }
                }
                conn.Close();
            }
        }
        protected List<FeedData> sqlLoad(string whereClause)
        {
            List<FeedData> lstReturn = new List<FeedData>();

            string combinedFields = combineFieldList(fieldsAll);
            string SELECT_SQL = string.Format("SELECT {0} FROM {1}", combinedFields, TABLE_NAME);

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                string sql = sql = SELECT_SQL;
                sql += " " + whereClause;

                using (SQLiteCommand cmd = new SQLiteCommand(sql, conn))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        FeedData o = null;
                        while (rdr.Read())
                        {
                            o = sqlParseResultSetObj(rdr);
                            lstReturn.Add(o);
                        }
                    }
                }
                conn.Close();
            }
            return lstReturn;
        }
        protected void sqlParseResultSet(SQLiteDataReader rdr)
        {
            this.FeedDataID = long.Parse(rdr[FIELD_ID].ToString());
            try
            {
                this.FeedID = Convert.ToInt32(rdr[FEED_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.FeedDataTypeID = Convert.ToInt32(rdr[FEED_DATA_TYPE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.DateCreated = DateTime.Parse(rdr[DATE_CREATED].ToString());
            }
            catch { }
            try
            {
                this.DateModified = DateTime.Parse(rdr[DATE_MODIFIED].ToString());
            }
            catch { }
            try
            {
                this.ClusterNum = Convert.ToInt32(rdr[CLUSTER_NUM].ToString().Trim());
            }
            catch { }
            this.DataRetsSource = rdr[DATA_RETS_SOURCE].ToString().Trim();
            this.DataXml = rdr[DATA_XML].ToString().Trim();
            this.DataJson = rdr[DATA_JSON].ToString().Trim();
            this.ListingKey = rdr[LISTING_KEY].ToString().Trim();

            this.Modificationtimestamp = rdr[MODIFICATIONTIMESTAMP].ToString().Trim();
            this.State = rdr[STATE].ToString().Trim();
            this.County = rdr[COUNTY].ToString().Trim();
            this.City = rdr[CITY].ToString().Trim();
            this.Mlsid = rdr[MLSID].ToString().Trim();
            this.Mlsname = rdr[MLSNAME].ToString().Trim();
            this.Mlsnumber = rdr[MLSNUMBER].ToString().Trim();
            this.ListingStatus = rdr[LISTING_STATUS].ToString().Trim();
            try
            {
                this.IsError = Convert.ToBoolean(rdr[IS_ERROR].ToString().Trim());
            }
            catch { }

            this.ErrorStr = rdr[ERROR_STR].ToString().Trim();
            try
            {
                this.IsUploading = Convert.ToBoolean(rdr[IS_UPLOADING].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsUploaded = Convert.ToBoolean(rdr[IS_UPLOADED].ToString().Trim());

            }
            catch { }
            try
            {
                this.DateBeginUploading = DateTime.Parse(rdr[DATE_BEGIN_UPLOADING].ToString());
            }
            catch { }
            try
            {
                this.DateEndUploading = DateTime.Parse(rdr[DATE_END_UPLOADING].ToString());

            }
            catch { }
            try
            {
                this.TotalTimeToUploadInMs = Convert.ToInt32(rdr[TOTAL_TIME_TO_UPLOAD_IN_MS].ToString().Trim());
            }
            catch { }

        }
        protected FeedData sqlParseResultSetObj(SQLiteDataReader rdr)
        {
            FeedData o = new FeedData();
            o.FeedDataID = long.Parse(rdr[FIELD_ID].ToString());
            try
            {
                o.FeedID = Convert.ToInt32(rdr[FEED_ID].ToString().Trim());
            }
            catch { }
            try
            {
                o.FeedDataTypeID = Convert.ToInt32(rdr[FEED_DATA_TYPE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                o.DateCreated = DateTime.Parse(rdr[DATE_CREATED].ToString());
            }
            catch { }
            try
            {
                o.DateModified = DateTime.Parse(rdr[DATE_MODIFIED].ToString());
            }
            catch { }
            try
            {
                o.ClusterNum = Convert.ToInt32(rdr[CLUSTER_NUM].ToString().Trim());
            }
            catch { }
            o.DataRetsSource = rdr[DATA_RETS_SOURCE].ToString().Trim();
            o.DataXml = rdr[DATA_XML].ToString().Trim();
            o.DataJson = rdr[DATA_JSON].ToString().Trim();
            o.ListingKey = rdr[LISTING_KEY].ToString().Trim();

            o.Modificationtimestamp = rdr[MODIFICATIONTIMESTAMP].ToString().Trim();
            o.State = rdr[STATE].ToString().Trim();
            o.County = rdr[COUNTY].ToString().Trim();
            o.City = rdr[CITY].ToString().Trim();
            o.Mlsid = rdr[MLSID].ToString().Trim();
            o.Mlsname = rdr[MLSNAME].ToString().Trim();
            o.Mlsnumber = rdr[MLSNUMBER].ToString().Trim();
            o.ListingStatus = rdr[LISTING_STATUS].ToString().Trim();
            try
            {
                o.IsError = Convert.ToBoolean(rdr[IS_ERROR].ToString().Trim());
            }
            catch { }

            o.ErrorStr = rdr[ERROR_STR].ToString().Trim();
            try
            {
                o.IsUploading = Convert.ToBoolean(rdr[IS_UPLOADING].ToString().Trim());
            }
            catch { }
            try
            {
                o.IsUploaded = Convert.ToBoolean(rdr[IS_UPLOADED].ToString().Trim());

            }
            catch { }
            try
            {
                o.DateBeginUploading = DateTime.Parse(rdr[DATE_BEGIN_UPLOADING].ToString());
            }
            catch { }
            try
            {
                o.DateEndUploading = DateTime.Parse(rdr[DATE_END_UPLOADING].ToString());

            }
            catch { }
            try
            {
                o.TotalTimeToUploadInMs = Convert.ToInt32(rdr[TOTAL_TIME_TO_UPLOAD_IN_MS].ToString().Trim());
            }
            catch { }


            return o;
        }

        protected int sqlMaxClusterNumber()
        {
            int max = 0;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();
                using (SQLiteCommand cmd = new SQLiteCommand(conn))
                {
                    cmd.CommandText = string.Format("select max(cluster_num) from feed_data where feed_id = {0} and feed_data_type_id = {1} and is_uploaded = 0", FeedID, FeedDataTypeID);
                    object val = cmd.ExecuteScalar();
                    max = int.Parse(val.ToString());
                }
                conn.Close();
            }
            return max;
        }
    }
}
