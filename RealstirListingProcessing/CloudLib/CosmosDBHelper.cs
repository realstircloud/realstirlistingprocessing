﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Threading.Tasks;

namespace Realstir.Engine.Listing.Cloud
{
    public class CosmosDBHelper
    {
        private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStacktrace = null;

        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }

        public void CreateDBIfNotExist(DocumentClient client, string dbName)
        {
            try
            {
                var db = client.CreateDatabaseIfNotExistsAsync(new Database { Id = dbName }).Result.Resource;
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                string err = string.Format("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                string err = string.Format("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
            }

        }

        public void CreateCollectionIfNotExist(DocumentClient client, string dbName, DocumentCollection docCollection)
        {
            try
            {
                var collection = client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(dbName),
                   docCollection, new RequestOptions { OfferThroughput = 1000 }).Result.Resource;
            }
            catch (DocumentClientException de)
            {
                Exception baseException = de.GetBaseException();
                string err = string.Format("{0} error occurred: {1}, Message: {2}", de.StatusCode, de.Message, baseException.Message);
            }
            catch (Exception e)
            {
                Exception baseException = e.GetBaseException();
                string err = string.Format("Error: {0}, Message: {1}", e.Message, baseException.Message);
            }
            finally
            {
            }

        }

    }
}
