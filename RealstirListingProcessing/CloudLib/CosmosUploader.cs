﻿using System.Net;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

using Realstir.Engine.Listing.Db.DAL;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using Microsoft.Azure.Documents.Spatial;
using System.Diagnostics;
using System.Text;

namespace Realstir.Engine.Listing.Cloud
{
    public class CosmosUploader
    {
        private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStacktrace = null;

        private DocumentClient client = null;
        public static string CosmosDBEndPointUrl = ConfigurationManager.AppSettings["CosmosDBEndPointUrl"];
        public static string CosmosDBPrimaryKey = ConfigurationManager.AppSettings["CosmosDBPrimaryKey"];
        public static string CosmosDBName = ConfigurationManager.AppSettings["CosmosDBName"];
        public static string CosmosDBListingCollectionName = ConfigurationManager.AppSettings["CosmosDBListingCollectionName"];
        public static string ImportToolExePath = ConfigurationManager.AppSettings["ImportToolExePath"];
        public static string ImportToolResultDirPath = ConfigurationManager.AppSettings["ImportToolResultDirPath"];

        public string configWorkingDir = ConfigurationManager.AppSettings["WorkingDir"];
        private string _strCacheDirpath = null;
        public static readonly string WORKING_CACHE_DIR_NAME = "Cache";
        public static readonly string WORKING_IMPORT_DIR_NAME = "Import";

        public static readonly string IMPORT_RESULT_SUCCESS_PATTERN = "Failed: 0";

        public List<States> StatesList = null;
        private CosmosDBHelper cosmosdbHelper = new CosmosDBHelper();
        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
        public string CacheDirpath
        {
            get { return _strCacheDirpath; }
        }
        public CosmosUploader()
        {
            init();
        }
        private void init()
        {
            try
            {
                _strCacheDirpath = configWorkingDir + System.IO.Path.DirectorySeparatorChar + WORKING_CACHE_DIR_NAME;

                this.client = new DocumentClient(new Uri(CosmosDBEndPointUrl), CosmosDBPrimaryKey);
                Sysconfig sysconfig = null;
                // create db if not exist
                sysconfig = new Sysconfig("SYSTEM_CONFIGURATION_COSMOS_DB_IS_CREATED");
                bool isDbCreated = Convert.ToBoolean(sysconfig.ConfigValue);
                if (!isDbCreated)
                {
                    cosmosdbHelper.CreateDBIfNotExist(this.client, CosmosDBName);
                    sysconfig.ConfigValue = "true";
                    sysconfig.Update();
                }

                StatesList = (new States()).Load(string.Empty);
                sysconfig = new Sysconfig("SYSTEM_CONFIGURATION_COSMOS_COLLECTIONS_IS_CREATED");
                bool isDbCollectionsCreated = Convert.ToBoolean(sysconfig.ConfigValue);
                if (!isDbCollectionsCreated)
                {
                    DocumentCollection docCollection = new DocumentCollection();
                    docCollection.Id = CosmosDBListingCollectionName;
                    docCollection.PartitionKey.Paths.Add("/PartitionKey");

                    cosmosdbHelper.CreateCollectionIfNotExist(client, CosmosDBName, docCollection);
                    sysconfig.ConfigValue = "true";
                    sysconfig.Update();
                }
            }
            catch
            {
                _hasError = true;
            }
        }
        public void UploadListings(Feed feed, int feedDataTypeID)
        {
            bool bError = false;
            try
            {
                if ( (feed != null) && (!_hasError))
                {

                    FeedData feedData = new FeedData() { FeedID = feed.FeedID, FeedDataTypeID = 1 };
                    string cacheDirPath = CacheDirpath + System.IO.Path.DirectorySeparatorChar + feed.LocalDirName;
                    string importDirPath = cacheDirPath + System.IO.Path.DirectorySeparatorChar + WORKING_IMPORT_DIR_NAME;
                    if (!Directory.Exists(importDirPath))
                    {
                        DirectoryInfo dirInfo = Directory.CreateDirectory(importDirPath);
                    }

                    int maxClusterSize = feedData.MaxClusterNumber();
                    if (maxClusterSize > 0)
                    {
                        ImportTaskObject importTaskObject = null;
                        List<ImportTaskObject> lstImportTaskObject = new List<ImportTaskObject>();
                        for (int i = 1; i <= maxClusterSize; i++)
                        {
                            importTaskObject = new ImportTaskObject();
                            importTaskObject.clusterNum = i;
                            importTaskObject.feed = feed;
                            importTaskObject.feedDataTypeID = feedDataTypeID;
                            importTaskObject.WorkingDirPath = importDirPath + System.IO.Path.DirectorySeparatorChar + i;
                            if (!Directory.Exists(importTaskObject.WorkingDirPath))
                            {
                                Directory.CreateDirectory(importTaskObject.WorkingDirPath);
                            }
                            lstImportTaskObject.Add(importTaskObject);
                        }
                        var pOptions = new ParallelOptions() { MaxDegreeOfParallelism = 1 };
                        Parallel.ForEach(lstImportTaskObject, pOptions, o =>
                        {
                            DoClusterUpload(o);
                        });

                        // check if all clusters are success
                        bool isSuccess = true;
                        StringBuilder sbErrors = new StringBuilder();
                        foreach(ImportTaskObject r in lstImportTaskObject)
                        {
                            // make sure all the clusters succeeded
                            if (r.HasError)
                            {
                                isSuccess = false;
                                sbErrors.Append(r.ToString() + Environment.NewLine);
                            }
                        }

                        if (isSuccess)
                        {

                        }
                        else
                        {

                        }
                    }

                }
            }
            catch (DocumentClientException de)
            {
                bError = true;
                _hasError = true;
                _errorStacktrace = de.StackTrace.ToString();
                _errorMessage = de.Message;
            }
            catch (Exception e)
            {
                bError = true;
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
            }
            finally
            {
                if (bError)
                {
                    try
                    {
                        feed.IsError = true;
                        feed.ErrorStr = _errorMessage + Environment.NewLine + _errorStacktrace;
                        feed.Save();
                        //long ID = _busFacCore.FeedCreateOrModify(feed);
                    }
                    catch { }
                }
                else
                {

                }
            }
        }

        public void DoClusterUpload(ImportTaskObject importTaskObject)
        {
            importTaskObject.StartUploadDate = DateTime.Now;
            importTaskObject.HasError = false;
            FeedData feedData = new FeedData();
            string criteria = string.Format("WHERE feed_id = {0} and feed_data_type_id = {1} and is_uploaded = 0 and cluster_num = {2} limit 10", importTaskObject.feed.FeedID, importTaskObject.feedDataTypeID, importTaskObject.clusterNum);
            List<FeedData> lstFeedData = feedData.Load(criteria);
            if ((lstFeedData != null) && (lstFeedData.Count > 0))
            {
                string jsonFilepath = null;
                ListingBundle listingBundle = null;
                string json = null;
                foreach (FeedData f in lstFeedData)
                {
                    dynamic results = JsonConvert.DeserializeObject<dynamic>(f.DataJson);

                    double geoLat = results.Listing.Location.Latitude;
                    double geoLong = results.Listing.Location.Longitude;

                    listingBundle = new ListingBundle();
                    listingBundle.Location = new Point(geoLong, geoLat);
                    listingBundle.PartitionKey = f.State;
                    listingBundle.RowID = f.ListingKey;
                    listingBundle.PostalCode = results.Listing.Address.PostalCode;
                    listingBundle.MlsNumber = results.Listing.MlsNumber;
                    listingBundle.jsonResult = results;

                    jsonFilepath = importTaskObject.WorkingDirPath + System.IO.Path.DirectorySeparatorChar + f.ListingKey + ".json";
                    json = JsonConvert.SerializeObject(listingBundle);
                    File.WriteAllText(jsonFilepath, json);
                }

                // run the migration tool command line

                var fileDate = DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss");
                string errorsFilePath = ImportToolResultDirPath + System.IO.Path.DirectorySeparatorChar + "importerrors-" + importTaskObject.feed.FeedID + "-" + importTaskObject.clusterNum + "_" + fileDate + ".csv";
                string importToolArgument = string.Format("/s:JsonFile /s.Files:{0}\\*.* /t:DocumentDB /t.ConnectionString:AccountEndpoint={1};AccountKey={2};Database={3} /t.IdField:RowID /t.Collection:{4} /t.PartitionKey:PartitionKey /t.UpdateExisting /ErrorLog:{5} /OverwriteErrorLog /ErrorDetails:All",
                    importTaskObject.WorkingDirPath, CosmosDBEndPointUrl, CosmosDBPrimaryKey, CosmosDBName, CosmosDBListingCollectionName, errorsFilePath);
                string result = ExecuteImportCommand(ImportToolExePath, importToolArgument, importTaskObject.WorkingDirPath);
                importTaskObject.Result = result;
                if ( !((!string.IsNullOrEmpty(result)) && (result.Contains(IMPORT_RESULT_SUCCESS_PATTERN))))
                {
                    importTaskObject.HasError = true;
                }
                importTaskObject.EndUploadDate = DateTime.Now;
            }

        }

        public string ExecuteImportCommand(string cmd, string arg, string workingDir)
        {
            string result = null;
            //Create process
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            pProcess.StartInfo.CreateNoWindow = true;

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = cmd;

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = arg;

            pProcess.StartInfo.UseShellExecute = false;

            //Set output of program to be written to process output stream
            pProcess.StartInfo.RedirectStandardOutput = true;

            //Optional
            pProcess.StartInfo.WorkingDirectory = workingDir;

            //Start the process
            pProcess.Start();

            //Get program output
            result = pProcess.StandardOutput.ReadToEnd();

            //Wait for process to finish
            pProcess.WaitForExit();

            return result;
           
        }

    }

    public class ListingBundle
    {
        [JsonProperty("PartitionKey")]
        public string PartitionKey { get; set; }
        [JsonProperty("RowID")]
        public string RowID { get; set; }
        [JsonProperty("location")]
        public Point Location { get; set; }
        [JsonProperty("PostalCode")]
        public string PostalCode { get; set; }
        [JsonProperty("MlsNumber")]
        public string MlsNumber { get; set; }
        [JsonProperty("Data")]
        public dynamic jsonResult { get; set; }

    }

    public class ImportTaskObject
    {
        public Feed feed { get; set; }
        public int clusterNum { get; set; }
        public int feedDataTypeID { get; set; }
        public string WorkingDirPath { get; set; }
        public bool HasError { get; set; }
        public string Result { get; set; }
        public DateTime StartUploadDate { get; set; }
        public DateTime EndUploadDate { get; set; }

        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("feed id:  " + feed.FeedID + "\n");
            sbReturn.Append("clusterNum:  " + clusterNum + "\n");
            sbReturn.Append("feedDataTypeID:  " + feedDataTypeID + "\n");
            sbReturn.Append("WorkingDirPath:  " + WorkingDirPath + "\n");
            sbReturn.Append("HasError:  " + HasError + "\n");
            sbReturn.Append("Result:  " + Result + "\n");
            sbReturn.Append("StartUploadDate:  " + StartUploadDate.ToLongDateString() + " " + StartUploadDate.ToLongTimeString() + "\n");
            sbReturn.Append("EndUploadDate:  " + EndUploadDate.ToLongDateString() + " " + EndUploadDate.ToLongTimeString() + "\n");

            return sbReturn.ToString();
        }
    }
}
