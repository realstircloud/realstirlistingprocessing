﻿CREATE TABLE dbversion
  (
     dbversion_id INTEGER NOT NULL PRIMARY KEY,
     date_created DATETIME NULL,
     major_num    INT DEFAULT 0,
     minor_num    INT DEFAULT 0,
     notes        TEXT NULL
  );

  CREATE TABLE [feed](
	[feed_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
	[feed_type_id] INTEGER NULL,
	[feed_state_id] INTEGER NULL,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[date_modified] [datetime] NULL,
	[date_processed] [datetime] NULL,
	[date_received] [datetime] NULL,
	[date_begin_processing] [datetime] NULL,
	[date_end_processing] [datetime] NULL,
	[date_deflated] [datetime] NULL,
	[date_clustered] [datetime] NULL,
	[date_published_text_str] [nvarchar](255) NULL,
	[record_count] [int] NULL,
	[is_error] [bit] NULL,
	[error_str] [text] NULL,
	[is_processing] [bit] NULL,
	[is_processed] [bit] NULL,
	[incoming_filename] [nvarchar](255) NULL,
	[blob_ref] [nvarchar](255) NULL,
	[blob_ref_uncompressed] [nvarchar](255) NULL,
	[incoming_content_length] [int] NULL,
	[incoming_content_type] [nvarchar](255) NULL,
	[processing_state] [int] NULL,
	[feed_xml_versionstamp] [nvarchar](255) NULL,
	[feed_xml_version] [nvarchar](255) NULL,
	[feed_xml_key] [nvarchar](255) NULL,
	[local_dir_name] [nvarchar](255) NULL,
	[local_dir_path] [nvarchar](255) NULL,
	[processed_record_count] [int] NULL,
	[number_of_clusters] [int] NULL,
	[date_begin_download] [datetime] NULL,
	[date_end_download] [datetime] NULL,
	[date_begin_deflate] [datetime] NULL,
	[date_end_deflate] [datetime] NULL,
	[date_begin_clustering] [datetime] NULL,
	[date_end_clustering] [datetime] NULL,
	[date_begin_uploading] [datetime] NULL,
	[date_end_uploading] [datetime] NULL
	);

	CREATE TABLE [sysconfig](
	[sysconfig_id] INTEGER NOT NULL PRIMARY KEY,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[date_modified] [datetime] NULL,
	[code] [text] NULL,
	[description] [text] NULL,
	[visible_code] [text] NULL,
	[config_value] [text] NULL
	);

	CREATE TABLE [feed_state](
	[feed_state_id] INTEGER NOT NULL PRIMARY KEY,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[code] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[visible_code] [nvarchar](255) NULL
	);

CREATE TABLE [feed_type](
	[feed_type_id] INTEGER NOT NULL PRIMARY KEY,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[code] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[visible_code] [nvarchar](255) NULL);

	CREATE TABLE [states](
	[states_id] INTEGER NOT NULL PRIMARY KEY,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[code] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[visible_code] [nvarchar](255) NULL
	);

	CREATE TABLE [syslog](
	[syslog_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
	[interaction_id] INTEGER NULL,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[date_modified] [datetime] NULL,
	[msgsource] [nvarchar](255) NULL,
	[msgaction] [nvarchar](255) NULL,
	[msgtxt] [text] NULL
	);

	CREATE TABLE [feed_data_type](
	[feed_data_type_id] INTEGER NOT NULL PRIMARY KEY,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[code] [nvarchar](255) NULL,
	[description] [nvarchar](255) NULL,
	[visible_code] [nvarchar](255) NULL);

	CREATE TABLE [feed_data](
	[feed_data_id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
	[feed_id] INTEGER NULL,
	[feed_data_type_id] INTEGER NULL,
	[date_created] TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	[date_modified] [datetime] NULL,
	[listing_key] [nvarchar](255) NULL,
	[cluster_num] INTEGER NULL,
	[data_rets_source] TEXT NULL,
	[data_xml] TEXT NULL,
	[data_json] TEXT NULL,
	[modificationtimestamp] [nvarchar](255) NULL,
	[state] [nvarchar](255) NULL,
	[county] [nvarchar](255) NULL,
	[city] [nvarchar](255) NULL,
	[mlsid] [nvarchar](255) NULL,
	[mlsname] [nvarchar](255) NULL,
	[mlsnumber] [nvarchar](255) NULL,
	[listing_status] [nvarchar](255) NULL,
	[is_error] [bit] NULL,
	[error_str] [text] NULL,
	[is_uploading] [bit] NULL,
	[is_uploaded] [bit] NULL,
	[date_begin_uploading] [datetime] NULL,
	[date_end_uploading] [datetime] NULL,
	[total_time_to_upload_in_ms] INTEGER NULL
	);

delete from feed_data_type;
INSERT INTO feed_data_type (feed_data_type_id, code, description, visible_code) VALUES (1, 'NEW', 'New Listings', 'New Listings');
INSERT INTO feed_data_type (feed_data_type_id, code, description, visible_code) VALUES (2, 'UPDATE', 'Update Listings', 'Update Listings');
INSERT INTO feed_data_type (feed_data_type_id, code, description, visible_code) VALUES (3, 'DELETE', 'Delete Listings', 'Delete Listings');

delete from feed_type;
INSERT INTO feed_type (feed_type_id, code, description, visible_code) VALUES (1, 'FEED_TYPE_LISTHUB', 'Listhub Listing Feed', 'Listhub Listing Feed');
INSERT INTO feed_type (feed_type_id, code, description, visible_code) VALUES (2, 'FEED_TYPE_HOMEJUNCTION', 'HomeJunction Listing Feed', 'HomeJunction Listing Feed');

delete from sysconfig;
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (1, 'SYSTEM_CONFIGURATION_LISTHUB_URI', 'Listhub feed URI', 'Listhub feed URI', 'https://feeds.listhub.com/pickup/');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (2, 'SYSTEM_CONFIGURATION_LISTHUB_USERNAME', 'Listhub feed Username', 'Listhub feed Username', 'realstir');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (3, 'SYSTEM_CONFIGURATION_LISTHUB_PASSWORD', 'Listhub feed Password', 'Listhub feed Password', 'A93uBi');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (4, 'SYSTEM_CONFIGURATION_LISTHUB_FILENAME', 'Listhub feed filename', 'Listhub feed filename', 'realstir.xml.gz');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (5, 'SYSTEM_CONFIGURATION_LISTHUB_CHANNELID', 'Listhub feed channel ID', 'Listhub feed channel ID', 'realstir');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (6, 'SYSTEM_CONFIGURATION_HOMEJUNCTION_API' , 'HomeJunction API.', 'HomeJunction API.', 'https://slipstream.homejunction.com/');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (7, 'SYSTEM_CONFIGURATION_HOMEJUNCTION_LICENSE' , 'HomeJunction License.', 'HomeJunction License.', 'D1BC-E23A-915B-D15E');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (8, 'SYSTEM_CONFIGURATION_HOMEJUNCTION_API_VERSION' , 'HomeJunction Version.', 'HomeJunction Version.', 'v1');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (9, 'SYSTEM_CONFIGURATION_LISTHUB_CLUSTER_SIZE_IN_LISTINGS', 'Listhub cluster size in listings', 'Listhub cluster size in listings', '5000');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (10, 'SYSTEM_CONFIGURATION_COSMOS_DB_IS_CREATED', 'Cosmosdb created', 'Cosmosdb created', 'false');
INSERT INTO sysconfig (sysconfig_id, code, description, visible_code, config_value) VALUES (11, 'SYSTEM_CONFIGURATION_COSMOS_COLLECTIONS_IS_CREATED', 'Cosmosdb collections created', 'Cosmosdb collections createds', 'false');


delete from feed_state;
INSERT INTO [feed_state] (feed_state_id, code, [description], visible_code) VALUES (1, 'RECEIVED', 'Received feed', 'Received data');
INSERT INTO [feed_state] (feed_state_id, code, [description], visible_code) VALUES (2, 'DEFLATED', 'Deflated feed', 'Deflated data');
INSERT INTO [feed_state] (feed_state_id, code, [description], visible_code) VALUES (3, 'LOCAL_STORAGE', 'Local stored feed', 'Local stored feed');
INSERT INTO [feed_state] (feed_state_id, code, [description], visible_code) VALUES (4, 'PROCESSED', 'Processed feed', 'Processed data');

delete from dbversion;
INSERT INTO [dbversion] (dbversion_id, major_num, minor_num,notes) VALUES (1, 1, 0,'Inital');

delete from states;
INSERT INTO states (states_id, code, [description], visible_code) VALUES (1, 'NJ', 'NJ', 'New Jersey');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (2, 'VA', 'VA', 'Virginia');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (3, 'PR', 'PR', 'Puerto Rico');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (4, 'MA', 'MA', 'Massachusetts');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (5, 'MS', 'MS', 'Mississippi');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (6, 'SD', 'SD', 'South Dakota');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (7, 'WI', 'WI', 'Wisconsin');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (8, 'NH', 'NH', 'New Hampshire');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (9, 'UT', 'UT', 'Utah');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (10, 'WV', 'WV', 'West Virginia');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (11, 'MT', 'MT', 'Montana');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (12, 'CA', 'CA', 'California');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (13, 'WY', 'WY', 'Wyoming');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (14, 'CT', 'CT', 'Connecticut');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (15, 'OK', 'OK', 'Oklahoma');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (16, 'NM', 'NM', 'New Mexico');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (17, 'RI', 'RI', 'Rhode Island');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (18, 'OR', 'OR', 'Oregon');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (19, 'NV', 'NV', 'Nevada');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (20, 'IN', 'IN', 'Indiana');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (21, 'AS', 'AS', 'American Samoa');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (22, 'ND', 'ND', 'North Dakota');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (23, 'MD', 'MD', 'Maryland');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (24, 'AR', 'AR', 'Arkansas');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (25, 'MI', 'MI', 'Michigan');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (26, 'KY', 'KY', 'Kentucky');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (27, 'GA', 'GA', 'Georgia');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (28, 'VI', 'VI', 'Virgin Islands');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (29, 'TN', 'TN', 'Tennessee');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (30, 'DC', 'DC', 'District of Columbia');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (31, 'VT', 'VT', 'Vermont');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (32, 'MP', 'MP', 'Northern Mariana Islands');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (33, 'IA', 'IA', 'Iowa');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (34, 'IL', 'IL', 'Illinois');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (35, 'PA', 'PA', 'Pennsylvania');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (36, 'NC', 'NC', 'North Carolina');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (37, 'OH', 'OH', 'Ohio');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (38, 'NE', 'NE', 'Nebraska');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (39, 'PW', 'PW', 'Palau');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (40, 'GU', 'GU', 'Guam');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (41, 'LA', 'LA', 'Louisiana');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (42, 'HI', 'HI', 'Hawaii');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (43, 'MH', 'MH', 'Marshall Islands');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (44, 'AZ', 'AZ', 'Arizona');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (45, 'FM', 'FM', 'Federated States of Micronesia');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (46, 'SC', 'SC', 'South Carolina');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (47, 'ME', 'ME', 'Maine');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (48, 'NY', 'NY', 'New York');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (49, 'AK', 'AK', 'Alaska');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (50, 'FL', 'FL', 'Florida');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (51, 'DE', 'DE', 'Delaware');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (52, 'CO', 'CO', 'Colorado');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (53, 'MN', 'MN', 'Minnesota');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (54, 'WA', 'WA', 'Washington');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (55, 'TX', 'TX', 'Texas');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (56, 'ID', 'ID', 'Idaho');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (57, 'KS', 'KS', 'Kansas');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (58, 'MO', 'MO', 'Missouri');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (59, 'AL', 'AL', 'Alabama');

INSERT INTO states (states_id, code, [description], visible_code) VALUES (60, 'NB', 'NB', 'NebraskaOld');

