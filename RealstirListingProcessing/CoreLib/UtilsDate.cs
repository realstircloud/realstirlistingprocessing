﻿using System;
using System.Globalization;

namespace Realstir.Engine.Listing.Core
{
    public class UtilsDate
    {
        /// <summary>
        /// The ISO 8601 format string.
        /// </summary>

        private const string Iso8601Format = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";

        //SortableDateTimePattern (ISO 8601)
        public static string ToIso8601(DateTime value)
        {
            //return value.ToUniversalTime().ToString(Iso8601Format, CultureInfo.InvariantCulture);
            return value.ToString(Iso8601Format, CultureInfo.InvariantCulture);
        }

        public static DateTime ParseIso8601(string value)
        {
            return DateTime.ParseExact(value,
                Iso8601Format, CultureInfo.InvariantCulture,
                    DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);
        }

        public static DateTime ConvertDate(string pStrDateValue)
        {
            DateTime dt = new DateTime();
            if (!string.IsNullOrEmpty(pStrDateValue))
            {
                bool bSuccess = DateTime.TryParse(pStrDateValue, out dt);
                if (bSuccess)
                {
                    dt = dt.ToUniversalTime();
                }
            }

            return dt;
        }
    }
}
