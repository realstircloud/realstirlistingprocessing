﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.Net.Http;
using System.Xml;
using Newtonsoft.Json;
using System.Linq;
using Realstir.Lib.Parser;
using System.Xml.Linq;

using Realstir.Engine.Listing.Db.DAL;
using Realstir.Engine.Listing.Cloud;
using static Realstir.Engine.Listing.Core.RetsObject;


namespace Realstir.Engine.Listing.Core
{
    public class HomeJunctionCore
    {
        private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStacktrace = null;

        public static readonly string WORKING_DOWNLOAD_DIR_NAME = "Download";
        public static readonly string WORKING_CACHE_DIR_NAME = "Cache";
        public static readonly string WORKING_ERROR_DIR_NAME = "Error";
        public static readonly int DOWNLOAD_BUFFERSIZE = 250 * 1024;
        public static readonly string PARSE_LISTING_ELEMENT_NAME = "Listing";
        public static readonly string PARSE_LISTINGS_ELEMENT_NAME = "Listings";
        public static readonly string TAG_VERSIONTIMESTAMP = "versionTimestamp";
        public static readonly string TAG_VERSION = "version";
        public static readonly string TAG_LISTINGSKEY = "listingsKey";
        public static readonly string CACHE_CURRENT_FILENAME = "listings_cache";
        public static readonly string CACHE_OTHER_OBJECTS = "OTHERS";

        public static readonly string FEED_INDEX_FILE_NAME = "index";

        private string API_ENDPOINT = null;
        private string API_LICENSE_KEY = null;
        private string API_TOKEN = string.Empty;
        private string API_VERSION = null;

        private DateTime standardDate;

        public string configWorkingDir = ConfigurationManager.AppSettings["WorkingDir"];
        private string _strCacheDirpath = null;

        private List<ListingCompareObject> _lstAllCompareObject = null;
        private Dictionary<string, ListingCompareObject> _dictAllCompareObject = null;
        private Dictionary<string, ListingCompareObject> _dictAllCompareObjectAudit = null;
        public Dictionary<string, ListingCompareObject> _dictAllCompareObjectAuditDeltaObjectNew = null;
        public Dictionary<string, ListingCompareObject> _dictAllCompareObjectAuditDeltaObjectUpdate = null;

        public string auditFilenameDate = null;
        public string auditFilename = null;
        private string _currentCacheDirpath = null;

        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
        public string CacheDirpath
        {
            get { return _strCacheDirpath; }
        }
        public List<ListingCompareObject> lstAllCompareObject
        {
            get { return _lstAllCompareObject; }
            set { _lstAllCompareObject = value; }
        }
        public Dictionary<string, ListingCompareObject> dictAllCompareObject
        {
            get { return _dictAllCompareObject; }
            set { _dictAllCompareObject = value; }
        }
        private void _init()
        {
            try
            {
                //_busFacListhubDownload = new BusFacListhubDownload();
                //_busFacListhubDownload.createInitialDirs();
                //_busFacCore = new BusFacCore(_busFacListhubDownload.listhubLocalConfig.ConnectionStringData);

                _strCacheDirpath = configWorkingDir + System.IO.Path.DirectorySeparatorChar + WORKING_CACHE_DIR_NAME;
                if (!Directory.Exists(_strCacheDirpath))
                {
                    Directory.CreateDirectory(_strCacheDirpath);
                }

                API_ENDPOINT = new Sysconfig("SYSTEM_CONFIGURATION_HOMEJUNCTION_API").ConfigValue;
                API_LICENSE_KEY = new Sysconfig("SYSTEM_CONFIGURATION_HOMEJUNCTION_LICENSE").ConfigValue;
                API_VERSION = new Sysconfig("SYSTEM_CONFIGURATION_HOMEJUNCTION_API_VERSION").ConfigValue;
                getToken();

            }
            catch (Exception e)
            {
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
            }
        }
        public void getToken()
        {
            string results = null;
            var api_auth = "/api/authenticate";
            var api_params = "?license=" + API_LICENSE_KEY;

            results = HTTP_GET(API_ENDPOINT + API_VERSION + api_auth + api_params).Result;

            RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(results);
            if (rootObject != null && rootObject.result != null && rootObject.result.token != null)
            {
                API_TOKEN = results = rootObject.result.token;
            }
        }
        public HomeJunctionCore()
        {
            _init();
        }
        public void DownloadFeed(string pStrMarket)
        {
            Feed feed = null;
            try
            {
                var fileDate = DateTime.UtcNow.ToString("dd-MMM-yyyy-HH-mm-ss");
                string saveAsFilename = string.Format("{0}_{1}_{2}",
                                    "HomeJunction",
                                    fileDate, "realstir.xml");
                string configWorkingDir = ConfigurationManager.AppSettings["WorkingDir"];
                string dirPath = configWorkingDir + System.IO.Path.DirectorySeparatorChar + WORKING_DOWNLOAD_DIR_NAME
                   + System.IO.Path.DirectorySeparatorChar + pStrMarket;
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                var path = dirPath + System.IO.Path.DirectorySeparatorChar + saveAsFilename;

                List<Feed> lstFeed = null;
                feed = new Feed();
                lstFeed = feed.Load("WHERE incoming_filename = \"" + saveAsFilename + "\"");
                if ((lstFeed != null) && (lstFeed.Count == 0))
                {
                    FeedType feedType = new FeedType("FEED_TYPE_HOMEJUNCTION");
                    feed = new Feed();

                    DateTime dateBeginDownload = DateTime.UtcNow;
                    DownloadFile(path, pStrMarket, ref feed);
                    DateTime dateEndDownload = DateTime.UtcNow;

                    feed.IncomingFilename = saveAsFilename;
                    feed.IncomingContentType = @"application/xml";
                    feed.DateReceived = DateTime.UtcNow;
                    feed.FeedTypeID = feedType.FeedTypeID;
                    feed.LocalDirPath = path;
                    feed.LocalDirName = fileDate;
                    feed.FeedStateID = 2;
                    feed.DateBeginDownload = dateBeginDownload;
                    feed.DateEndDownload = dateEndDownload;
                    feed.Save();

                }


            }
            catch (Exception e)
            {
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;

            }
            finally
            {
                if (_hasError)
                {
                    try
                    {
                        //Syslog syslog = new Syslog();
                        //syslog.Msgaction = "ERROR_DOWNLOAD_LISTHUB";
                        //syslog.Msgsource = "DownloadListhubFeed";
                        //syslog.Msgtxt = _errorMessage + " " + _errorStacktrace;
                        //long syslogID = _busFacCore.SyslogCreateOrModify(syslog);
                    }
                    catch { }
                }
            }
        }

        private void DownloadFile(string saveAsPath, string pStrMarket, ref Feed feed)
        {
            RetsObject.Listings lstListings = new RetsObject.Listings();
            lstListings.AllListing = new List<RetsObject.Listing>();
            lstListings.ListingsKey = UtilsDate.ToIso8601(DateTime.UtcNow) + " -- " + Guid.NewGuid();
            lstListings.Commons = "http://rets.org/xsd/RETSCommons";
            lstListings.Lang = "en-us";
            lstListings.Version = "1.0";
            lstListings.VersionTimestamp = UtilsDate.ToIso8601(DateTime.UtcNow);
            lstListings.SchemaLocation = "http://rets.org/xsd/Syndication/2012-03/Syndication.xsd";

            DateTime dateBeginDownload = DateTime.UtcNow;
            RetsObject.Listings listings = null;

            HomeJunctionObject lstHomejunction = null;
            listings = getListings("1", pStrMarket, ref lstHomejunction);

            //for (int i = 1; i < 112; i++)
            if ((lstHomejunction != null) && (lstHomejunction.success))
            {
                if (pStrMarket != "fysbo")
                {
                    for (int i = 1; i <= lstHomejunction.result.paging.count; i++)
                    {
                        listings = getListings(i.ToString(), pStrMarket, ref lstHomejunction);
                        if ((lstHomejunction != null) && (lstHomejunction.success))
                        {
                            lstListings.AllListing.AddRange(listings.AllListing);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    lstListings.AllListing.AddRange(listings.AllListing);
                }

            }


            var _XML = "";
            using (StreamWriter w = File.AppendText(saveAsPath))
            {
                XmlSerializer xsSubmit = new XmlSerializer(lstListings.GetType());
                using (var sw = new StringWriter())
                {
                    using (var xw = XmlWriter.Create(sw))
                    {
                        xsSubmit.Serialize(xw, lstListings);
                    }
                    if (sw.ToString().IndexOf("<?xml version=\"1.0\" encoding=\"utf-16\"?>") != -1)
                    {
                        _XML = sw.ToString();
                        int indB = _XML.IndexOf("<Listings");
                        if (indB != -1)
                        {
                            _XML = "<?xml version=\"1.0\"?>" + _XML.Remove(0, indB);
                        }
                        //_XML = _XML.Replace("<?xml version=\"1.0\"", string.Empty);
                        //_XML = _XML.Replace(" encoding=\"utf-16\"?>", string.Empty);
                        //_XML = sw.ToString().Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", string.Empty);
                    }
                    else
                    {
                        _XML = sw.ToString();
                    }
                }
                _XML = UtilsXml.ConditionHomeJunctionXML(_XML);
                w.WriteLine(_XML);
            }
            if ((lstListings != null) && (lstListings.AllListing != null))
            {
                feed.RecordCount = lstListings.AllListing.Count;
            }
            if (!string.IsNullOrEmpty(_XML))
            {
                feed.IncomingContentLength = _XML.Length;
            }
        }

        private async Task<string> HTTP_GET(string URL, List<KeyValuePair<string, string>> Headers = null)
        {
            string Res = null;
            using (HttpClient client = new HttpClient())
            {
                if (Headers != null)
                {
                    foreach (var element in Headers)
                    {
                        client.DefaultRequestHeaders.Add(element.Key, element.Value);
                    }
                }

                using (HttpResponseMessage response = await client.GetAsync(URL).ConfigureAwait(false))
                using (HttpContent content = response.Content)
                {
                    Res = await content.ReadAsStringAsync();
                }
            }

            return Res;
        }

        public RetsObject.Listings getListings(string pPageNumber, string pMarket, ref HomeJunctionObject lstHomejunction)
        {
            RetsObject.Listings RootObject = new RetsObject.Listings();
            RetsObject.Listing RootListing = null;
            RetsObject.ModificationTimestamp retsModificationTimestamp = null;
            RootObject.AllListing = new List<RetsObject.Listing>();

            lstHomejunction = null;
            string results = null;
            if (pMarket != "fysbo")
            {
                var api_auth = "/listings/search";
                var api_params = "?market=" + pMarket + "&details=true&extended=false&features=true&images=true&status=active,pending&pageSize=100&pageNumber=" + pPageNumber;
                var PostHeaders = new List<KeyValuePair<string, string>>();
                PostHeaders.Add(new KeyValuePair<string, string>("HJI-Slipstream-Token", API_TOKEN));
                results = HTTP_GET(API_ENDPOINT + API_VERSION + api_auth + api_params, PostHeaders).Result;
            }
            else
            {
                // fysbo
                string uploadPath = configWorkingDir + System.IO.Path.DirectorySeparatorChar + "Upload";
                if (!Directory.Exists(uploadPath))
                {
                    Directory.CreateDirectory(uploadPath);
                }
                FileInfo fileInfo = GetNewestFile(uploadPath);
                if (fileInfo != null)
                {
                    results = File.ReadAllText(fileInfo.FullName);
                    File.Move(fileInfo.FullName, fileInfo.FullName + ".pr");
                }
            }
            if (results != null)
            {
                lstHomejunction = JsonConvert.DeserializeObject<HomeJunctionObject>(results);
                int cnt = 0;
                foreach (var _item in lstHomejunction.result.listings)
                {
                    cnt++;
                    DateTime epoch = new DateTime(1970, 1, 1);
                    DateTime ts = epoch.AddSeconds(_item.lastUpdated);
                    string modificationTimestamp = UtilsDate.ToIso8601(ts);
                    RootListing = convertHomeJunctionToRetsFormat(_item).Listing;
                    retsModificationTimestamp = new RetsObject.ModificationTimestamp() { Text = modificationTimestamp };
                    RootListing.ModificationTimestamp = retsModificationTimestamp;
                    RootListing.MlsId = RootListing.MlsId.ToUpper();
                    RootListing.MlsName = RootListing.MlsName.ToUpper();
                    RootListing.MlsNumber = RootListing.MlsNumber.ToUpper();
                    RootListing.ListingKey = "3yd-" + RootListing.MlsName + "-" + RootListing.MlsNumber;
                    if (!string.IsNullOrEmpty(RootListing.PropertyType.Text))
                    {
                        if (RootListing.PropertyType.Text == "Rental")
                        {
                            RootListing.ListingCategory = "Rental";
                        }
                        else
                        {
                            RootListing.ListingCategory = "Purchase";
                        }
                    }
                    // first name field contains both first and last separate
                    if ((RootListing.ListingParticipants != null) && (RootListing.ListingParticipants.Participant != null))
                    {
                        try
                        {
                            if ((!string.IsNullOrEmpty(RootListing.ListingParticipants.Participant.FirstName))
                          && (RootListing.ListingParticipants.Participant.FirstName.IndexOf(" ") != -1))
                            {
                                string[] split = null;
                                split = RootListing.ListingParticipants.Participant.FirstName.Split(new Char[] { ' ' });
                                RootListing.ListingParticipants.Participant.FirstName = split[0];
                                RootListing.ListingParticipants.Participant.LastName = split[split.Length - 1];
                            }
                        }
                        catch { }

                    }
                    RootObject.AllListing.Add(RootListing);
                }
            }

            return RootObject;
        }

        public FileInfo GetNewestFile(string directory)
        {
            FileInfo file = null;
            string pattern = "Fysbo*.json";
            var dirInfo = new DirectoryInfo(directory);
            try
            {
                file = (from f in dirInfo.GetFiles(pattern) orderby f.LastWriteTime descending select f).First();
            }
            catch { }
            return file;
        }
        public RetsObject.Listings convertHomeJunctionToRetsFormat(HomeJunctionObject.Listing pHomeJunctionListing)
        {
            HomeJunctionObject.Listing homeJunctionListing = pHomeJunctionListing;
            RetsObject.Listings retsObject = null;

            try
            {
                // Init
                retsObject = new RetsObject.Listings();
                retsObject.Listing = new RetsObject.Listing();
                retsObject.Listing.Address = new RetsObject.Address();
                retsObject.Listing.ListPrice = new RetsObject.ListPrice();
                retsObject.Listing.PropertyType = new RetsObject.PropertyType();
                retsObject.Listing.Photos = new RetsObject.Photos();
                retsObject.Listing.Photos.Photo = new List<RetsObject.Photo>();
                retsObject.Listing.Location = new RetsObject.Location();
                retsObject.Listing.Offices = new RetsObject.Offices();
                retsObject.Listing.Offices.Office = new RetsObject.Office();
                retsObject.Listing.PropertySubType = new RetsObject.PropertySubType();
                retsObject.Listing.Location.Community = new RetsObject.Community();
                retsObject.Listing.Location.Community.Schools = new RetsObject.Schools();
                retsObject.Listing.DetailedCharacteristics = new RetsObject.DetailedCharacteristics();
                retsObject.Listing.DetailedCharacteristics.CoolingSystems = new RetsObject.CoolingSystems();
                retsObject.Listing.DetailedCharacteristics.ExteriorTypes = new RetsObject.ExteriorTypes();
                retsObject.Listing.DetailedCharacteristics.ExteriorTypes.ExteriorType = new List<string>();
                retsObject.Listing.DetailedCharacteristics.ArchitectureStyle = new RetsObject.ArchitectureStyle();
                retsObject.Listing.Location.Community.Subdivision = new RetsObject.Subdivision();
                retsObject.Listing.Location.Community.Schools.School = new List<RetsObject.School>();
                retsObject.Listing.ListingParticipants = new RetsObject.ListingParticipants();
                retsObject.Listing.ListingParticipants.Participant = new RetsObject.Participant();
                retsObject.Listing.Disclaimer = new RetsObject.Disclaimer();
                // --

                // -------------------------------------------------------------------------------------------
                // -------------------------------------------------------------------------------------------
                // ------------------------------------ Converter --------------------------------------------

                // Listing Key
                retsObject.Listing.ListingKey = "3yd-" + homeJunctionListing.market + "-" + homeJunctionListing.id;

                // Address
                if (homeJunctionListing.address != null)
                {
                    retsObject.Listing.Address.City = homeJunctionListing.address.city;
                    retsObject.Listing.Address.FullStreetAddress = homeJunctionListing.address.street;
                    retsObject.Listing.Address.StateOrProvince = homeJunctionListing.address.state;
                    retsObject.Listing.Address.PostalCode = homeJunctionListing.address.zip;
                }
                retsObject.Listing.Location.County = homeJunctionListing.county;

                // Price
                retsObject.Listing.ListPrice.Text = homeJunctionListing.listPrice.ToString();

                // Bedrooms
                retsObject.Listing.Bedrooms = homeJunctionListing.beds.ToString();

                // Bathrooms
                if (homeJunctionListing.baths != null)
                {
                    retsObject.Listing.Bathrooms = homeJunctionListing.baths.total.ToString();
                }

                // Property Type
                retsObject.Listing.PropertyType.Text = homeJunctionListing.listingType;

                // Property Sub Type
                retsObject.Listing.PropertySubType.Text = homeJunctionListing.propertyType;

                // Photos
                if (homeJunctionListing.images != null)
                {
                    foreach (var photo in homeJunctionListing.images)
                    {
                        RetsObject.Photo pic = new RetsObject.Photo();
                        pic.MediaURL = photo;

                        retsObject.Listing.Photos.Photo.Add(pic);
                    }
                }


                // Year built
                retsObject.Listing.YearBuilt = homeJunctionListing.yearBuilt.ToString();

                // Description
                retsObject.Listing.ListingDescription = homeJunctionListing.description;

                // Coordinates
                if (homeJunctionListing.coordinates != null)
                {
                    retsObject.Listing.Location.Latitude = homeJunctionListing.coordinates.latitude.ToString();
                    retsObject.Listing.Location.Longitude = homeJunctionListing.coordinates.longitude.ToString();
                }

                // LotSize
                if (homeJunctionListing.lotSize != null)
                {
                    retsObject.Listing.LotSize = homeJunctionListing.lotSize.acres.ToString();
                }

                // Status
                retsObject.Listing.ListingStatus = homeJunctionListing.status;

                // Area
                retsObject.Listing.LivingArea = homeJunctionListing.size.ToString();

                // MLS
                retsObject.Listing.MlsId = homeJunctionListing.market;
                retsObject.Listing.MlsName = homeJunctionListing.market;
                retsObject.Listing.MlsNumber = homeJunctionListing.id;

                // Listing Date
                standardDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                standardDate = standardDate.AddSeconds(homeJunctionListing.listingDate).ToLocalTime();
                retsObject.Listing.ListingDate = standardDate.ToShortDateString();

                // Agent
                if (homeJunctionListing.listingAgent != null)
                {
                    retsObject.Listing.ListingParticipants.Participant.ParticipantId = homeJunctionListing.listingAgent.id;
                    retsObject.Listing.ListingParticipants.Participant.FirstName = homeJunctionListing.listingAgent.name;
                    retsObject.Listing.ListingParticipants.Participant.OfficePhone = homeJunctionListing.listingAgent.phone;
                }

                // Style
                if (homeJunctionListing.style != null)
                {
                    retsObject.Listing.DetailedCharacteristics.ArchitectureStyle.Text = homeJunctionListing.style.ToString();
                }

                // Office
                if (homeJunctionListing.listingOffice != null)
                {
                    retsObject.Listing.Offices.Office.OfficeId = homeJunctionListing.listingOffice.id;
                    retsObject.Listing.Offices.Office.Name = homeJunctionListing.listingOffice.name;
                    retsObject.Listing.Offices.Office.PhoneNumber = homeJunctionListing.listingOffice.phone;
                }

                // Subdivision
                retsObject.Listing.Location.Community.Subdivision.Text = homeJunctionListing.subdivision;

                // Listing URL
                retsObject.Listing.ListingURL = homeJunctionListing.tourURL;

                // Features
                if (homeJunctionListing.features != null)
                {
                    if (homeJunctionListing.features.Cooling != null)
                    {
                        retsObject.Listing.DetailedCharacteristics.CoolingSystems.CoolingSystem = homeJunctionListing.features.Cooling[0];
                    }

                    retsObject.Listing.DetailedCharacteristics.HasSecuritySystem = (homeJunctionListing.features.SecuritySystem != null) ? "true" : "false";

                    if (homeJunctionListing.features.ExteriorFeatures != null)
                    {
                        foreach (var item in homeJunctionListing.features.ExteriorFeatures)
                        {
                            retsObject.Listing.DetailedCharacteristics.ExteriorTypes.ExteriorType.Add(item);
                        }
                    }

                    // Schools
                    if (homeJunctionListing.features.Schools != null)
                    {
                        foreach (var item in homeJunctionListing.features.Schools)
                        {
                            RetsObject.School school = new RetsObject.School();
                            school.District = new RetsObject.District();

                            school.District.Text = item;
                            retsObject.Listing.Location.Community.Schools.School.Add(school);
                        }
                    }
                }

                // -------------------------------------------------------------------------------------------
                // -------------------------------------------------------------------------------------------

                // ----- NULL ------

                // 
                retsObject.Listing.Disclaimer.Text = "";
                retsObject.Listing.FullBathrooms = "0";
                retsObject.Listing.HalfBathrooms = "0";
            }
            catch (Exception e)
            {
                Console.WriteLine("Error : " + e.Message);
            }

            return retsObject;
        }

        public void ClusterFeed()
        {
            Feed feed = null;
            bool bError = false;
            string feedFilepath = null;
            string statusFilepath = null;
            try
            {
                long lFeedStateID = 2;
                FeedType feedType = new FeedType("FEED_TYPE_HOMEJUNCTION");
                feed = FeedGetLast(feedType.FeedTypeID);
                if ((feed != null) && (feed.FeedStateID == lFeedStateID))
                {
                    string sourceFilepath = feed.LocalDirPath;
                    if (File.Exists(sourceFilepath))
                    {
                        int recordCount = 0;
                        int clusterCount = 0;
                        string Versionstamp = null;
                        string Version = null;
                        string Listingskey = null;
                        string cacheDirPath = CacheDirpath + System.IO.Path.DirectorySeparatorChar + feed.LocalDirName;
                        if (!Directory.Exists(cacheDirPath))
                        {
                            DirectoryInfo dirInfo = Directory.CreateDirectory(cacheDirPath);
                        }
                        DateTime dateBeginClustering = DateTime.UtcNow;

                        feed.DateBeginClustering = dateBeginClustering;
                        feed.DateClustered = DateTime.UtcNow;
                        feed.Save();
                        feed.Load();

                        statusFilepath = cacheDirPath + System.IO.Path.DirectorySeparatorChar + "status.txt";
                        SplitDownload(sourceFilepath, ref recordCount, ref Versionstamp, ref Version, ref Listingskey, cacheDirPath, ref clusterCount, feed, "FEED_TYPE_HOMEJUNCTION");
                        DateTime dateEndClustering = DateTime.UtcNow;
                        feed.IsError = false;
                        feed.ErrorStr = null;
                        feed.FeedStateID = 3;
                        feed.NumberOfClusters = clusterCount;
                        feed.RecordCount = recordCount;
                        feed.FeedXmlVersionstamp = Versionstamp;
                        feed.FeedXmlVersion = Version;
                        feed.FeedXmlKey = Listingskey;
                        feed.DateEndClustering = dateEndClustering;
                        feed.Save();
                    }
                    else
                    {
                        // file does not exist. Something is wrong
                    }
                }
            }
            catch (Exception e)
            {
                bError = true;
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
                if (statusFilepath != null)
                {
                    try
                    {
                        string statusMsg = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + ":  "
                            + e.Message + Environment.NewLine + e.StackTrace.ToString() + Environment.NewLine;
                        File.AppendAllText(statusFilepath, statusMsg);
                    }
                    catch { }
                }

            }
            finally
            {
                if ((feed != null) && (bError))
                {
                    try
                    {
                        feed.IsError = true;
                        feed.ErrorStr = _errorMessage + Environment.NewLine + _errorStacktrace;
                        feed.Save();
                        //long ID = _busFacCore.FeedCreateOrModify(feed);
                    }
                    catch { }
                }
            }
        }

        public Feed FeedGetLast(long pLngFeedTypeID = 0, long pLngFeedStateID = 0)
        {
            Feed feed = null;
            Feed feedTmp = new Feed();
            string sqlWhere = null;
            if ((pLngFeedStateID > 0) && (pLngFeedTypeID > 0))
            {
                sqlWhere = string.Format("WHERE feed_type_id = {0} AND feed_state_id = {1}", pLngFeedTypeID, pLngFeedStateID);
            }
            else if (pLngFeedTypeID > 0)
            {
                sqlWhere = string.Format("WHERE feed_type_id = {0}", pLngFeedTypeID);
            }
            else if (pLngFeedStateID > 0)
            {
                sqlWhere = string.Format("WHERE feed_state_id = {0}", pLngFeedStateID);
            }

            List<Feed> lstFeed = feedTmp.Load(sqlWhere);
            if ((lstFeed != null) && (lstFeed.Count > 0))
            {
                feed = (Feed)lstFeed[lstFeed.Count - 1];
            }
            //List<Feed> lstFeed = new Feed()
            //ArrayList items = null;
            //_log("GET", "Received Get Request Feed Last");
            //EnumFeed enumFeed = new EnumFeed();
            //enumFeed.FeedTypeID = pLngFeedTypeID;
            //enumFeed.FeedStateID = pLngFeedStateID;
            //items = FeedGetList(enumFeed);
            //if ((items != null) && (items.Count > 0))
            //{
            //    feed = (Feed)items[items.Count - 1];
            //}
            return feed;
        }

        public void SplitDownload(string pStrSourceFilepath, ref int recordCount, ref string pStrVersionstamp, ref string pStrVersion, ref string pStrListingskey, string pStrCacheDirPath, ref int clusterCount, Feed feed, string pStrFeedTypeCode = "FEED_TYPE_LISTHUB")
        {
            recordCount = 0;
            clusterCount = 0;
            pStrVersionstamp = null;
            pStrVersion = null;
            pStrListingskey = null;
            bool bHeaderProcessed = false;
            List<ListingObject> lstListingObject = new List<ListingObject>();
            List<ListingCompareObject> lstListingCompareObjectAll = new List<ListingCompareObject>();
            ListingCompareObject listingCompareObject = null;
            List<string> containerListJustAdded = new List<string>();
            string listingXml = null;
            int clusterSize = Convert.ToInt32(new Sysconfig("SYSTEM_CONFIGURATION_LISTHUB_CLUSTER_SIZE_IN_LISTINGS").ConfigValue);

            States states = new States();

            List<States> lstStates = states.Load(string.Empty);

            bool DoInclude = false;
            string county = null;
            string city = null;
            string StateOrProvince = null;
            Dictionary<string, long> countyCountDictionary = new Dictionary<string, long>();
            Dictionary<string, long> cityCountDictionary = new Dictionary<string, long>();
            Dictionary<string, long> StateOrProvinceCountDictionary = new Dictionary<string, long>();
            Dictionary<string, List<ListingObject>> dataDict = new Dictionary<string, List<ListingObject>>();
            List<ListingObject> dict = null;
            //BusFacCore busFacCore = new BusFacCore();
            bool isListingProcessed = false;
            //BusFacListhubUtils busFacListhubUtils = new BusFacListhubUtils();
            NanoXMLDocument xml = null;
            NanoXMLNode nd = null;
            NanoXMLNode ndAddress = null;
            string listingkey = null;
            string ModificationTimestamp = null;
            string MlsId = null;
            string MlsName = null;
            string MlsNumber = null;
            string ListingStatus = null;
            string statusFilepath = pStrCacheDirPath + System.IO.Path.DirectorySeparatorChar + "status.txt";
            string processedFilepath = pStrCacheDirPath + System.IO.Path.DirectorySeparatorChar + "processed.txt";
            string xmlDirpath = pStrCacheDirPath + System.IO.Path.DirectorySeparatorChar + "XML";
            string xmlFilepath = null;
            string deleteDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "DELETE";
            string newDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "NEW";
            string updateDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "UPDATE";
            string compressedDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "COMPRESSED";
            string allDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "ALL";
            byte[] btCompressedXml;

            string statusMsg = null;
            String incomingStatus = null;
            XDocument xDoc = null;
            XElement xe = null;
            string xmlDirpathTmp = null;
            int nNewCount = 0;
            int nUpdateCount = 0;
            int nDeleteCount = 0;

            int nNewFileInd = 0;
            int nUpdateFileInd = 0;
            int nDeleteFileInd = 0;

            FeedData feedData = null;

            if (!Directory.Exists(xmlDirpath))
            {
                Directory.CreateDirectory(xmlDirpath);
            }
            if (!Directory.Exists(deleteDirpath))
            {
                Directory.CreateDirectory(deleteDirpath);
            }
            if (!Directory.Exists(newDirpath))
            {
                Directory.CreateDirectory(newDirpath);
            }
            if (!Directory.Exists(updateDirpath))
            {
                Directory.CreateDirectory(updateDirpath);
            }
            if (!Directory.Exists(compressedDirpath))
            {
                Directory.CreateDirectory(compressedDirpath);
            }
            if (!Directory.Exists(allDirpath))
            {
                Directory.CreateDirectory(allDirpath);
            }
            string allSubDirpath = allDirpath + System.IO.Path.DirectorySeparatorChar + "1";
            if (!Directory.Exists(allSubDirpath))
            {
                Directory.CreateDirectory(allSubDirpath);
            }

            int noKeyCount = 0;

            string fileContent = File.ReadAllText(pStrSourceFilepath);
            int indB = 0;
            int indE = 0;
            listingXml = null;
            string listingKeywordB = "<Listing>";
            string listingKeywordE = "</Listing>";
            while (indB < fileContent.Length)
            {
                indB = fileContent.IndexOf(listingKeywordB, indB);
                if (indB != -1)
                {
                    indE = fileContent.IndexOf(listingKeywordE, indB);
                    listingXml = fileContent.Substring(indB, indE - indB + listingKeywordE.Length);
                    indB = indE + +listingKeywordE.Length;
                }
                else
                {
                    break;
                }

                DoInclude = true;
                StateOrProvince = null;
                county = null;
                city = null;

                xml = new NanoXMLDocument(listingXml);

                listingkey = GetNanoXmlValue(xml.RootNode, "ListingKey");
                if (string.IsNullOrEmpty(listingkey))
                {
                    noKeyCount++;
                    continue;
                }
                ModificationTimestamp = GetNanoXmlValue(xml.RootNode, "ModificationTimestamp");

                isListingProcessed = IsListingProcessed(listingkey, ModificationTimestamp, ref incomingStatus, "FEED_TYPE_HOMEJUNCTION");

                nd = xml.RootNode["Location"];
                if (nd != null)
                {
                    try
                    {
                        county = GetNanoXmlValue(nd, "County");
                    }
                    catch { county = null; }
                }
                ndAddress = xml.RootNode["Address"];
                if (ndAddress != null)
                {
                    try
                    {
                        city = GetNanoXmlValue(ndAddress, "commons:City");
                    }
                    catch { city = null; }
                    try
                    {
                        StateOrProvince = GetNanoXmlValue(ndAddress, "commons:StateOrProvince");
                    }
                    catch { StateOrProvince = null; }
                }

                if ((recordCount % clusterSize) == 0)
                {
                    statusMsg = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + ":  " + recordCount + Environment.NewLine;
                    File.AppendAllText(statusFilepath, statusMsg);
                }
                recordCount++;

                MlsId = GetNanoXmlValue(xml.RootNode, "MlsId");
                MlsName = GetNanoXmlValue(xml.RootNode, "MlsName");
                MlsNumber = GetNanoXmlValue(xml.RootNode, "MlsNumber");
                ListingStatus = GetNanoXmlValue(xml.RootNode, "ListingStatus");

                if ((!isListingProcessed) && (!string.IsNullOrEmpty(incomingStatus)))
                {
                    feedData = new FeedData() { FeedID = feed.FeedID, ListingKey = listingkey };

                    if (incomingStatus == "NEW")
                    {
                        if ((nNewCount % clusterSize) == 0)
                        {
                            nNewFileInd++;
                            newDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "NEW" + System.IO.Path.DirectorySeparatorChar + nNewFileInd;
                            if (!Directory.Exists(newDirpath))
                            {
                                Directory.CreateDirectory(newDirpath);
                            }
                        }
                        nNewCount++;
                        xmlDirpathTmp = newDirpath;
                        feedData.FeedDataTypeID = 1;
                        feedData.ClusterNum = nNewFileInd;
                    }
                    if (incomingStatus == "UPDATE")
                    {
                        if ((nUpdateCount % clusterSize) == 0)
                        {
                            nUpdateFileInd++;
                            updateDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "UPDATE" + System.IO.Path.DirectorySeparatorChar + nUpdateFileInd;
                            if (!Directory.Exists(updateDirpath))
                            {
                                Directory.CreateDirectory(updateDirpath);
                            }

                        }
                        nUpdateCount++;
                        xmlDirpathTmp = updateDirpath;
                        feedData.FeedDataTypeID = 2;
                        feedData.ClusterNum = nUpdateFileInd;
                    }
                    //xmlFilepath = xmlDirpathTmp + System.IO.Path.DirectorySeparatorChar + listingkey + ".xml.txt";
                    //File.WriteAllText(xmlFilepath, listingXml);
                    feedData.DataRetsSource = listingXml;

                    xmlFilepath = xmlDirpathTmp + System.IO.Path.DirectorySeparatorChar + listingkey + ".xml";
                    xDoc = XDocument.Parse(listingXml);
                    UtilsXml.RemoveAttributes(xDoc.Root);
                    xe = UtilsXml.stripNS(xDoc.Root);
                    //xe.Save(xmlFilepath);
                    //xe.Save(allSubDirpath + System.IO.Path.DirectorySeparatorChar + listingkey + ".xml");
                    feedData.DataXml = xe.ToString();

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(feedData.DataXml);
                    string json = JsonConvert.SerializeXmlNode(doc);
                    feedData.DataJson = json;
                    doc = null;

                    feedData.Modificationtimestamp = ModificationTimestamp;
                    feedData.State = StateOrProvince;
                    feedData.County = county;
                    feedData.City = city;
                    feedData.Mlsid = MlsId;
                    feedData.Mlsname = MlsName;
                    feedData.Mlsnumber = MlsNumber;
                    feedData.ListingStatus = ListingStatus;
                    feedData.Save();
                    xDoc = null;

                }

                listingCompareObject = new ListingCompareObject();
                listingCompareObject.ListingKey = listingkey;
                listingCompareObject.ModificationTimestamp = ModificationTimestamp;
                listingCompareObject.ModificationTimestampDate = UtilsDate.ConvertDate(ModificationTimestamp);
                listingCompareObject.Id = recordCount;
                listingCompareObject.StateOrProvince = StateOrProvince;
                lstListingCompareObjectAll.Add(listingCompareObject);

                //xDoc = XDocument.Parse(listingXml);
                //UtilsXml.RemoveAttributes(xDoc.Root);
                //xe = UtilsXml.stripNS(xDoc.Root);
                //xe.Save(allSubDirpath + System.IO.Path.DirectorySeparatorChar + listingkey + ".xml");
                xDoc = null;

            }

            string currentIndexFilepath = null;
            currentIndexFilepath = pStrCacheDirPath + System.IO.Path.DirectorySeparatorChar + FEED_INDEX_FILE_NAME;
            using (StreamWriter file = File.CreateText(currentIndexFilepath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, lstListingCompareObjectAll);
            }

            // find the deletes
            LoadFeedIndex("FEED_TYPE_HOMEJUNCTION");
            if ((lstListingCompareObjectAll.Count > 0) && (dictAllCompareObject != null)
                    && (dictAllCompareObject.Count > 0))
            {
                string deleteFilepath = null;
                Dictionary<string, ListingCompareObject> dictAllCompareObjectAll = lstListingCompareObjectAll.Distinct().ToDictionary(i => i.ListingKey, i => i);
                nDeleteCount = 0;
                // check previous and compare with current
                foreach (var item in dictAllCompareObject)
                {
                    feedData = new FeedData() { FeedDataTypeID = 3, FeedID = feed.FeedID };
                    if (!dictAllCompareObjectAll.ContainsKey(item.Key))
                    {
                        if ((nDeleteCount % clusterSize) == 0)
                        {
                            nDeleteFileInd++;
                            deleteDirpath = xmlDirpath + System.IO.Path.DirectorySeparatorChar + "DELETE" + System.IO.Path.DirectorySeparatorChar + nDeleteFileInd;
                            if (!Directory.Exists(deleteDirpath))
                            {
                                Directory.CreateDirectory(deleteDirpath);
                            }
                        }
                        //deleteFilepath = deleteDirpath + System.IO.Path.DirectorySeparatorChar + item.Key;
                        //File.WriteAllText(deleteFilepath, string.Empty);
                        nDeleteCount++;
                        feedData.ListingKey = item.Key;
                        feedData.ClusterNum = nDeleteFileInd;
                        feedData.Save();
                    }
                }
            }


            statusMsg = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + ":  " + recordCount + Environment.NewLine;
            File.AppendAllText(statusFilepath, statusMsg);
            File.AppendAllText(processedFilepath, "Path:  " + pStrCacheDirPath);
            File.AppendAllText(processedFilepath, "New Count:  " + nNewCount);
            File.AppendAllText(processedFilepath, "Update Count:  " + nUpdateCount);
            File.AppendAllText(processedFilepath, "Delete Count:  " + nDeleteCount);
            File.AppendAllText(processedFilepath, "Total Activated Count (New + Updated):  " + (nUpdateCount + nNewCount));
            File.AppendAllText(processedFilepath, "Total Record Count:  " + recordCount);

            lstListingCompareObjectAll.Clear();
            lstListingCompareObjectAll = null;

        }

        private string GetNanoXmlValue(NanoXMLNode nd, string pStrElement)
        {
            string s = null;
            try
            {
                s = nd[pStrElement].Value;
            }
            catch { s = null; }
            return s;
        }

        public bool IsListingProcessed(string pStrListingKey, string pStrModificationTimestamp, ref String pStrIncomingStatus, string pStrFeedTypeCode = "FEED_TYPE_LISTHUB")
        {
            bool b = true;
            pStrIncomingStatus = null;
            try
            {
                if (_dictAllCompareObjectAuditDeltaObjectNew == null)
                {
                    _dictAllCompareObjectAuditDeltaObjectNew = new Dictionary<string, ListingCompareObject>();
                }

                if (_dictAllCompareObjectAuditDeltaObjectUpdate == null)
                {
                    _dictAllCompareObjectAuditDeltaObjectUpdate = new Dictionary<string, ListingCompareObject>();
                }

                if (_dictAllCompareObject == null)
                {
                    LoadFeedIndex(pStrFeedTypeCode);
                }
                if (_dictAllCompareObject != null)
                {
                    ListingCompareObject o = null;
                    if (!_dictAllCompareObject.ContainsKey(pStrListingKey))
                    {
                        pStrIncomingStatus = "NEW";
                        b = false;
                    }
                    else if ((_dictAllCompareObjectAudit != null) && (_dictAllCompareObjectAudit.Count > 0) && (!_dictAllCompareObjectAudit.ContainsKey(pStrListingKey)))
                    {
                        pStrIncomingStatus = "NEW";
                        b = false;
                        _dictAllCompareObjectAuditDeltaObjectNew.Add(pStrListingKey, new ListingCompareObject() { ListingKey = pStrListingKey });
                    }
                    else
                    {
                        o = _dictAllCompareObject[pStrListingKey];

                        if (o.ModificationTimestamp != pStrModificationTimestamp)
                        {
                            pStrIncomingStatus = "UPDATE";
                            b = false;
                        }

                        if ((_dictAllCompareObjectAudit != null) && (_dictAllCompareObjectAudit.Count > 0))
                        {
                            o = _dictAllCompareObjectAudit[pStrListingKey];
                            if (o.ModificationTimestamp != pStrModificationTimestamp)
                            {
                                pStrIncomingStatus = "UPDATE";
                                b = false;
                                _dictAllCompareObjectAuditDeltaObjectUpdate.Add(pStrListingKey, new ListingCompareObject() { ListingKey = pStrListingKey });
                            }
                        }
                    }
                }
                else
                {
                    pStrIncomingStatus = "NEW";
                    b = false;
                }
            }
            catch { b = false; }
            return b;
        }

        public void LoadFeedIndex(string pStrFeedTypeCode)
        {
            bool bError = false;
            try
            {
                FeedType feedType = new FeedType(pStrFeedTypeCode);
                Feed feedIndex = FeedGetPrevToLastConfirmed(feedType.FeedTypeID, 3);
                if (feedIndex != null)
                {
                    string cacheDirpath = null;
                    cacheDirpath = configWorkingDir + System.IO.Path.DirectorySeparatorChar + WORKING_CACHE_DIR_NAME;
                    //if (pStrFeedTypeCode == "FEED_TYPE_LISTHUB")
                    //{
                    //    cacheDirpath = cacheDirpath.Replace("HomeJunction", "Listhub");
                    //}

                    _currentCacheDirpath = cacheDirpath + System.IO.Path.DirectorySeparatorChar + feedIndex.LocalDirName;
                    string indexPath = cacheDirpath + System.IO.Path.DirectorySeparatorChar + feedIndex.LocalDirName + System.IO.Path.DirectorySeparatorChar + FEED_INDEX_FILE_NAME;
                    if (File.Exists(indexPath))
                    {
                        List<ListingCompareObject> tmpLstAllCompareObject = null;
                        // deserialize JSON directly from a file
                        using (StreamReader file = File.OpenText(indexPath))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            tmpLstAllCompareObject = (List<ListingCompareObject>)serializer.Deserialize(file, typeof(List<ListingCompareObject>));
                        }
                        if (tmpLstAllCompareObject != null)
                        {
                            if (_lstAllCompareObject == null)
                            {
                                _lstAllCompareObject = new List<ListingCompareObject>();
                            }
                            _lstAllCompareObject.AddRange(tmpLstAllCompareObject);
                            _dictAllCompareObject = lstAllCompareObject.Distinct().ToDictionary(i => i.ListingKey, i => i);

                        }
                    }

                    string indexPathAuditCsv = cacheDirpath + System.IO.Path.DirectorySeparatorChar + "Audit" + System.IO.Path.DirectorySeparatorChar + "Audit.csv";
                    if (File.Exists(indexPathAuditCsv))
                    {
                        _dictAllCompareObjectAudit = new Dictionary<string, ListingCompareObject>();
                        ListingCompareObject listingCompareObject = null;
                        using (StreamReader r = new StreamReader(indexPathAuditCsv))
                        {
                            string[] split = null;
                            string line;
                            int cnt = 0;
                            while ((line = r.ReadLine()) != null)
                            {
                                cnt++;
                                split = line.Split(new Char[] { ',' });
                                listingCompareObject = new ListingCompareObject();
                                listingCompareObject.Id = Convert.ToInt32(split[0]);
                                listingCompareObject.ListingKey = split[1];
                                listingCompareObject.ModificationTimestamp = split[2];
                                try
                                {
                                    listingCompareObject.ModificationTimestampDate = UtilsDate.ConvertDate(split[2]);
                                }
                                catch { listingCompareObject.ModificationTimestampDate = new DateTime(); }

                                listingCompareObject.StateOrProvince = split[4];
                                _dictAllCompareObjectAudit.Add(split[1], listingCompareObject);
                            }
                        }
                        auditFilenameDate = DateTime.Now.ToString("dd-MMM-yyyy-HH-mm-ss");
                        auditFilename = "Audit" + System.IO.Path.DirectorySeparatorChar + "Audit-" + auditFilenameDate + ".csv";
                        string indexPathAuditCsvDest = cacheDirpath + System.IO.Path.DirectorySeparatorChar + "Audit" + System.IO.Path.DirectorySeparatorChar + "Audit-" + auditFilenameDate + ".csv";
                        File.Move(indexPathAuditCsv, indexPathAuditCsvDest);
                    }

                }

            }
            catch (Exception e)
            {
                bError = true;
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;

            }
            finally
            {
                if (bError)
                {
                }
            }
        }

        public Feed FeedGetPrevToLastConfirmed(long pLngFeedTypeID, long pLngFeedStateID)
        {
            Feed feed = null;
            List<Feed> lstFeed = (new Feed()).Load("WHERE feed_type_id = " + pLngFeedTypeID);
            Feed tmpFeed = null;
            if ((lstFeed != null) && (lstFeed.Count > 0))
            {
                for (int i = lstFeed.Count - 2; i >= 0; i--)
                {
                    tmpFeed = lstFeed[i];
                    if (tmpFeed.FeedStateID == pLngFeedStateID)
                    {
                        feed = tmpFeed;
                        break;
                    }
                }

            }
            return feed;
        }

        public void Upload()
        {
            Feed feed = null;
            bool bError = false;
            try
            {
                long lFeedStateID = 3;
                FeedType feedType = new FeedType("FEED_TYPE_HOMEJUNCTION");
                feed = FeedGetLast(feedType.FeedTypeID);
                if ((feed != null) && (feed.FeedStateID == lFeedStateID))
                {
                    CosmosUploader cosmosUploader = new CosmosUploader();
                    feed.DateBeginUploading = DateTime.UtcNow;
                    feed.Save();
                    feed.Load();
                    cosmosUploader.UploadListings(feed, 1);
                    feed.DateEndUploading = DateTime.UtcNow;
                    feed.IsError = false;
                    feed.ErrorStr = null;
                    feed.FeedStateID = 4;
                    feed.Save();
                }
            }
            catch (Exception e)
            {
                bError = true;
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
            }
            finally
            {
                if ((feed != null) && (bError))
                {
                    try
                    {
                        feed.IsError = true;
                        feed.ErrorStr = _errorMessage + Environment.NewLine + _errorStacktrace;
                        feed.Save();
                        //long ID = _busFacCore.FeedCreateOrModify(feed);
                    }
                    catch { }
                }
            }
        }

        public void RegionData()
        {
            try
            {
            }
            catch (Exception e)
            {
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
            }
            finally
            {
            }
        }


    }

 
}
