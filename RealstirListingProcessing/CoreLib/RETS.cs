﻿

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Realstir.Engine.Listing.Core
{
    public class Customer
    {
        public string name { get; set; }
        public string officeName { get; set; }
        public string emailAddress { get; set; }
    }

    public class Access
    {
        public bool listings { get; set; }
        public bool sales { get; set; }
    }

    public class Market
    {
        public string id { get; set; }
        public Access access { get; set; }
    }

    public class Result
    {
        public string license { get; set; }
        public string licenseKey { get; set; }
        public string token { get; set; }
        public int expires { get; set; }
        public Customer customer { get; set; }
        public List<Market> markets { get; set; }
    }

    public class RootObject
    {
        public bool success { get; set; }
        public Result result { get; set; }
    }

    public class HomeJunctionObject
    {
        public bool success { get; set; }
        public Result result { get; set; }

        public class Paging
        {
            public int count { get; set; }
            public int number { get; set; }
            public int size { get; set; }
        }

        public class Result
        {
            public int total { get; set; }
            public Paging paging { get; set; }
            public Listing[] listings { get; set; }
        }

        public class Listing
        {
            public string id { get; set; }
            public string market { get; set; }
            public string geoType { get; set; }
            public Schools schools { get; set; }
            public Listingoffice listingOffice { get; set; }
            public Address address { get; set; }
            public string distressed { get; set; }
            public string description { get; set; }
            public int listPrice { get; set; }
            public int yearBuilt { get; set; }
            public int daysOnHJI { get; set; }
            public int lastUpdated { get; set; }
            public string county { get; set; }
            public string listingType { get; set; }
            public Baths baths { get; set; }
            public Listingagent listingAgent { get; set; }
            public Lotsize lotSize { get; set; }
            public string style { get; set; }
            public string subdivision { get; set; }
            public string propertyType { get; set; }
            public int size { get; set; }
            public Coordinates coordinates { get; set; }
            public int listingDate { get; set; }
            public string tourURL { get; set; }
            public int daysOnMarket { get; set; }
            public string area { get; set; }
            public int beds { get; set; }
            public string status { get; set; }
            public string[] images { get; set; }
            public Features features { get; set; }
        }

        public class Schools
        {
            public string district { get; set; }
        }

        public class Listingoffice
        {
            public string id { get; set; }
            public string name { get; set; }
            public string phone { get; set; }
        }

        public class Address
        {
            public string deliveryLine { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string zip { get; set; }
            public string street { get; set; }
        }

        public class Baths
        {
            public int total { get; set; }
            public int full { get; set; }
            public int half { get; set; }
        }

        public class Listingagent
        {
            public string id { get; set; }
            public string name { get; set; }
            public string phone { get; set; }
        }

        public class Lotsize
        {
            public float sqft { get; set; }
            public float acres { get; set; }
        }

        public class Coordinates
        {
            public float latitude { get; set; }
            public float longitude { get; set; }
        }

        public class Features
        {
            public string[] Bathrooms { get; set; }
            public string[] Bedrooms { get; set; }
            public string[] FirstBedroom { get; set; }
            public string[] SecondBedroom { get; set; }
            public string[] ThirdBedroom { get; set; }
            public string[] House { get; set; }
            public string[] Cooling { get; set; }
            public string[] DiningRoom { get; set; }
            public string[] ExteriorFeatures { get; set; }
            public string[] Fireplaces { get; set; }
            public string[] Floors { get; set; }
            public string[] FamilyRoom { get; set; }
            public string[] Water { get; set; }
            public string[] Heating { get; set; }
            public string[] InteriorFeatures { get; set; }
            public string[] Kitchen { get; set; }
            public string[] Laundry { get; set; }
            public string[] Listing { get; set; }
            public string[] Location { get; set; }
            public string[] Lot { get; set; }
            public string[] LivingRoom { get; set; }
            public string[] Miscellaneous { get; set; }
            public string[] Parking { get; set; }
            public string[] Pool { get; set; }
            public string[] Property { get; set; }
            public string[] Roof { get; set; }
            public string[] Schools { get; set; }
            public string[] SecuritySystem { get; set; }
            public string[] Utilities { get; set; }
        }

    }

    public class RetsObject
    {
        [XmlRoot(ElementName = "Address", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Address
        {
            [XmlElement(ElementName = "preference-order", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string Preferenceorder { get; set; }
            [XmlElement(ElementName = "address-preference-order", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string Addresspreferenceorder { get; set; }
            [XmlElement(ElementName = "FullStreetAddress", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string FullStreetAddress { get; set; }
            [XmlElement(ElementName = "UnitNumber", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string UnitNumber { get; set; }
            [XmlElement(ElementName = "City", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string City { get; set; }
            [XmlElement(ElementName = "StateOrProvince", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string StateOrProvince { get; set; }
            [XmlElement(ElementName = "PostalCode", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string PostalCode { get; set; }
            [XmlElement(ElementName = "Country", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string Country { get; set; }
        }

        [XmlRoot(ElementName = "ListPrice", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ListPrice
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "ListPriceLow", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ListPriceLow
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "AlternateListPrice", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class AlternateListPrice
        {
            [XmlAttribute(AttributeName = "currencyCode", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string CurrencyCode { get; set; }
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "AlternateListPriceLow", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class AlternateListPriceLow
        {
            [XmlAttribute(AttributeName = "currencyCode", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string CurrencyCode { get; set; }
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "AlternatePrice", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class AlternatePrice
        {
            [XmlElement(ElementName = "AlternateListPrice", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public AlternateListPrice AlternateListPrice { get; set; }
            [XmlElement(ElementName = "AlternateListPriceLow", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public AlternateListPriceLow AlternateListPriceLow { get; set; }
        }

        [XmlRoot(ElementName = "AlternatePrices", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class AlternatePrices
        {
            [XmlElement(ElementName = "AlternatePrice", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public AlternatePrice AlternatePrice { get; set; }
        }

        [XmlRoot(ElementName = "PropertyType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class PropertyType
        {
            [XmlAttribute(AttributeName = "otherDescription")]
            public string OtherDescription { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "PropertySubType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class PropertySubType
        {
            [XmlAttribute(AttributeName = "otherDescription")]
            public string OtherDescription { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "PermitAddressOnInternet", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class PermitAddressOnInternet
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "VOWAddressDisplay", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class VOWAddressDisplay
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "VOWAutomatedValuationDisplay", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class VOWAutomatedValuationDisplay
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "VOWConsumerComment", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class VOWConsumerComment
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "MarketingInformation", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class MarketingInformation
        {
            [XmlElement(ElementName = "PermitAddressOnInternet", Namespace = "http://rets.org/xsd/RETSCommons")]
            public PermitAddressOnInternet PermitAddressOnInternet { get; set; }
            [XmlElement(ElementName = "VOWAddressDisplay", Namespace = "http://rets.org/xsd/RETSCommons")]
            public VOWAddressDisplay VOWAddressDisplay { get; set; }
            [XmlElement(ElementName = "VOWAutomatedValuationDisplay", Namespace = "http://rets.org/xsd/RETSCommons")]
            public VOWAutomatedValuationDisplay VOWAutomatedValuationDisplay { get; set; }
            [XmlElement(ElementName = "VOWConsumerComment", Namespace = "http://rets.org/xsd/RETSCommons")]
            public VOWConsumerComment VOWConsumerComment { get; set; }
        }

        [XmlRoot(ElementName = "MediaModificationTimestamp", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class MediaModificationTimestamp
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Photo", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Photo
        {
            [XmlElement(ElementName = "MediaModificationTimestamp", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public MediaModificationTimestamp MediaModificationTimestamp { get; set; }
            [XmlElement(ElementName = "MediaURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaURL { get; set; }
            [XmlElement(ElementName = "MediaCaption", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaCaption { get; set; }
            [XmlElement(ElementName = "MediaDescription", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaDescription { get; set; }
        }

        [XmlRoot(ElementName = "Photos", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Photos
        {
            [XmlElement(ElementName = "Photo", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<Photo> Photo { get; set; }
        }

        [XmlRoot(ElementName = "Participant", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Participant
        {
            [XmlElement(ElementName = "ParticipantKey", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ParticipantKey { get; set; }
            [XmlElement(ElementName = "ParticipantId", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ParticipantId { get; set; }
            [XmlElement(ElementName = "FirstName", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string FirstName { get; set; }
            [XmlElement(ElementName = "LastName", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string LastName { get; set; }
            [XmlElement(ElementName = "Role", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Role { get; set; }
            [XmlElement(ElementName = "PrimaryContactPhone", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string PrimaryContactPhone { get; set; }
            [XmlElement(ElementName = "OfficePhone", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string OfficePhone { get; set; }
            [XmlElement(ElementName = "Email", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Email { get; set; }
            [XmlElement(ElementName = "Fax", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Fax { get; set; }
            [XmlElement(ElementName = "WebsiteURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string WebsiteURL { get; set; }
        }

        [XmlRoot(ElementName = "ListingParticipants", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ListingParticipants
        {
            [XmlElement(ElementName = "Participant", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Participant Participant { get; set; }
        }

        [XmlRoot(ElementName = "VirtualTour", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class VirtualTour
        {
            [XmlElement(ElementName = "MediaModificationTimestamp", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public MediaModificationTimestamp MediaModificationTimestamp { get; set; }
            [XmlElement(ElementName = "MediaURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaURL { get; set; }
            [XmlElement(ElementName = "MediaCaption", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaCaption { get; set; }
            [XmlElement(ElementName = "MediaDescription", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaDescription { get; set; }
        }

        [XmlRoot(ElementName = "VirtualTours", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class VirtualTours
        {
            [XmlElement(ElementName = "VirtualTour", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public VirtualTour VirtualTour { get; set; }
        }

        [XmlRoot(ElementName = "Video", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Video
        {
            [XmlElement(ElementName = "MediaModificationTimestamp", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public MediaModificationTimestamp MediaModificationTimestamp { get; set; }
            [XmlElement(ElementName = "MediaURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaURL { get; set; }
            [XmlElement(ElementName = "MediaCaption", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaCaption { get; set; }
            [XmlElement(ElementName = "MediaDescription", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MediaDescription { get; set; }
        }

        [XmlRoot(ElementName = "Videos", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Videos
        {
            [XmlElement(ElementName = "Video", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Video Video { get; set; }
        }

        [XmlRoot(ElementName = "OfficeCode", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class OfficeCode
        {
            [XmlElement(ElementName = "OfficeCodeId", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string OfficeCodeId { get; set; }
        }

        [XmlRoot(ElementName = "Office", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Office
        {
            [XmlElement(ElementName = "OfficeKey", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string OfficeKey { get; set; }
            [XmlElement(ElementName = "OfficeId", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string OfficeId { get; set; }
            [XmlElement(ElementName = "Level", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Level { get; set; }
            [XmlElement(ElementName = "OfficeCode", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public OfficeCode OfficeCode { get; set; }
            [XmlElement(ElementName = "Name", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Name { get; set; }
            [XmlElement(ElementName = "CorporateName", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string CorporateName { get; set; }
            [XmlElement(ElementName = "BrokerId", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string BrokerId { get; set; }
            [XmlElement(ElementName = "PhoneNumber", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string PhoneNumber { get; set; }
            [XmlElement(ElementName = "Address", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "Website", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Website { get; set; }
        }

        [XmlRoot(ElementName = "Offices", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Offices
        {
            [XmlElement(ElementName = "Office", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Office Office { get; set; }
        }

        [XmlRoot(ElementName = "Brokerage", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Brokerage
        {
            [XmlElement(ElementName = "Name", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Phone", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Phone { get; set; }
            [XmlElement(ElementName = "Email", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Email { get; set; }
            [XmlElement(ElementName = "WebsiteURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string WebsiteURL { get; set; }
            [XmlElement(ElementName = "LogoURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string LogoURL { get; set; }
            [XmlElement(ElementName = "Address", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "Franchise", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Franchise
        {
            [XmlElement(ElementName = "Name", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Name { get; set; }
        }

        [XmlRoot(ElementName = "Builder", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Builder
        {
            [XmlElement(ElementName = "Name", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Phone", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Phone { get; set; }
            [XmlElement(ElementName = "Fax", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Fax { get; set; }
            [XmlElement(ElementName = "Email", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Email { get; set; }
            [XmlElement(ElementName = "WebsiteURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string WebsiteURL { get; set; }
            [XmlElement(ElementName = "Address", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Address Address { get; set; }
        }

        [XmlRoot(ElementName = "Subdivision", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class Subdivision
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "District", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class District
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "School", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class School
        {
            [XmlElement(ElementName = "Name", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string Name { get; set; }
            [XmlElement(ElementName = "SchoolCategory", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string SchoolCategory { get; set; }
            [XmlElement(ElementName = "District", Namespace = "http://rets.org/xsd/RETSCommons")]
            public District District { get; set; }
            [XmlElement(ElementName = "Description", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string Description { get; set; }
        }

        [XmlRoot(ElementName = "Schools", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class Schools
        {
            [XmlElement(ElementName = "School", Namespace = "http://rets.org/xsd/RETSCommons")]
            public List<School> School { get; set; }
        }

        [XmlRoot(ElementName = "Community", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Community
        {
            [XmlElement(ElementName = "Subdivision", Namespace = "http://rets.org/xsd/RETSCommons")]
            public Subdivision Subdivision { get; set; }
            [XmlElement(ElementName = "Schools", Namespace = "http://rets.org/xsd/RETSCommons")]
            public Schools Schools { get; set; }
        }

        [XmlRoot(ElementName = "Neighborhood", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Neighborhood
        {
            [XmlElement(ElementName = "Name", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Description", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Description { get; set; }
        }

        [XmlRoot(ElementName = "Neighborhoods", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Neighborhoods
        {
            [XmlElement(ElementName = "Neighborhood", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<Neighborhood> Neighborhood { get; set; }
        }

        [XmlRoot(ElementName = "Location", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Location
        {
            [XmlElement(ElementName = "Latitude", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Latitude { get; set; }
            [XmlElement(ElementName = "Longitude", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Longitude { get; set; }
            [XmlElement(ElementName = "Elevation", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Elevation { get; set; }
            [XmlElement(ElementName = "Directions", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Directions { get; set; }
            [XmlElement(ElementName = "GeocodeOptions", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string GeocodeOptions { get; set; }
            [XmlElement(ElementName = "County", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string County { get; set; }
            [XmlElement(ElementName = "ParcelId", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ParcelId { get; set; }
            [XmlElement(ElementName = "Community", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Community Community { get; set; }
            [XmlElement(ElementName = "Neighborhoods", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Neighborhoods Neighborhoods { get; set; }
        }

        [XmlRoot(ElementName = "OpenHouse", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class OpenHouse
        {
            [XmlElement(ElementName = "Date", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Date { get; set; }
            [XmlElement(ElementName = "StartTime", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string StartTime { get; set; }
            [XmlElement(ElementName = "EndTime", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string EndTime { get; set; }
            [XmlElement(ElementName = "Description", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Description { get; set; }
        }

        [XmlRoot(ElementName = "OpenHouses", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class OpenHouses
        {
            [XmlElement(ElementName = "OpenHouse", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public OpenHouse OpenHouse { get; set; }
        }

        [XmlRoot(ElementName = "Tax", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Tax
        {
            [XmlElement(ElementName = "Year", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Year { get; set; }
            [XmlElement(ElementName = "Amount", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Amount { get; set; }
            [XmlElement(ElementName = "TaxDescription", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string TaxDescription { get; set; }
        }

        [XmlRoot(ElementName = "Taxes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Taxes
        {
            [XmlElement(ElementName = "Tax", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<Tax> Tax { get; set; }
        }

        [XmlRoot(ElementName = "ExpenseValue", Namespace = "http://rets.org/xsd/RETSCommons")]
        public class ExpenseValue
        {
            [XmlAttribute(AttributeName = "currencyPeriod", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string CurrencyPeriod { get; set; }
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Expense", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Expense
        {
            [XmlElement(ElementName = "ExpenseCategory", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string ExpenseCategory { get; set; }
            [XmlElement(ElementName = "ExpenseValue", Namespace = "http://rets.org/xsd/RETSCommons")]
            public ExpenseValue ExpenseValue { get; set; }
        }

        [XmlRoot(ElementName = "Expenses", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Expenses
        {
            [XmlElement(ElementName = "Expense", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<Expense> Expense { get; set; }
        }

        [XmlRoot(ElementName = "Appliances", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Appliances
        {
            [XmlElement(ElementName = "Appliance", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<string> Appliance { get; set; }
        }

        [XmlRoot(ElementName = "ArchitectureStyle", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ArchitectureStyle
        {
            [XmlAttribute(AttributeName = "otherDescription")]
            public string OtherDescription { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "CoolingSystems", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class CoolingSystems
        {
            [XmlElement(ElementName = "CoolingSystem", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string CoolingSystem { get; set; }
        }

        [XmlRoot(ElementName = "ExteriorTypes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ExteriorTypes
        {
            [XmlElement(ElementName = "ExteriorType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<string> ExteriorType { get; set; }
        }

        [XmlRoot(ElementName = "FloorCoverings", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class FloorCoverings
        {
            [XmlElement(ElementName = "FloorCovering", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<string> FloorCovering { get; set; }
        }

        [XmlRoot(ElementName = "HeatingFuels", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class HeatingFuels
        {
            [XmlElement(ElementName = "HeatingFuel", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HeatingFuel { get; set; }
        }

        [XmlRoot(ElementName = "HeatingSystems", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class HeatingSystems
        {
            [XmlElement(ElementName = "HeatingSystem", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HeatingSystem { get; set; }
        }

        [XmlRoot(ElementName = "RoofTypes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class RoofTypes
        {
            [XmlElement(ElementName = "RoofType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string RoofType { get; set; }
        }

        [XmlRoot(ElementName = "Rooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Rooms
        {
            [XmlElement(ElementName = "Room", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public List<string> Room { get; set; }
        }

        [XmlRoot(ElementName = "ViewTypes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ViewTypes
        {
            [XmlElement(ElementName = "ViewType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ViewType { get; set; }
        }

        [XmlRoot(ElementName = "DetailedCharacteristics", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class DetailedCharacteristics
        {
            [XmlElement(ElementName = "Appliances", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Appliances Appliances { get; set; }
            [XmlElement(ElementName = "ArchitectureStyle", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ArchitectureStyle ArchitectureStyle { get; set; }
            [XmlElement(ElementName = "HasAttic", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasAttic { get; set; }
            [XmlElement(ElementName = "HasBarbecueArea", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasBarbecueArea { get; set; }
            [XmlElement(ElementName = "HasBasement", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasBasement { get; set; }
            [XmlElement(ElementName = "BuildingUnitCount", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string BuildingUnitCount { get; set; }
            [XmlElement(ElementName = "IsCableReady", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string IsCableReady { get; set; }
            [XmlElement(ElementName = "HasCeilingFan", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasCeilingFan { get; set; }
            [XmlElement(ElementName = "CondoFloorNum", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string CondoFloorNum { get; set; }
            [XmlElement(ElementName = "CoolingSystems", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public CoolingSystems CoolingSystems { get; set; }
            [XmlElement(ElementName = "HasDeck", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasDeck { get; set; }
            [XmlElement(ElementName = "HasDisabledAccess", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasDisabledAccess { get; set; }
            [XmlElement(ElementName = "HasDock", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasDock { get; set; }
            [XmlElement(ElementName = "HasDoorman", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasDoorman { get; set; }
            [XmlElement(ElementName = "HasDoublePaneWindows", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasDoublePaneWindows { get; set; }
            [XmlElement(ElementName = "HasElevator", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasElevator { get; set; }
            [XmlElement(ElementName = "ExteriorTypes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ExteriorTypes ExteriorTypes { get; set; }
            [XmlElement(ElementName = "HasFireplace", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasFireplace { get; set; }
            [XmlElement(ElementName = "FloorCoverings", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public FloorCoverings FloorCoverings { get; set; }
            [XmlElement(ElementName = "HasGarden", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasGarden { get; set; }
            [XmlElement(ElementName = "HasGatedEntry", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasGatedEntry { get; set; }
            [XmlElement(ElementName = "HasGreenhouse", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasGreenhouse { get; set; }
            [XmlElement(ElementName = "HeatingFuels", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public HeatingFuels HeatingFuels { get; set; }
            [XmlElement(ElementName = "HeatingSystems", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public HeatingSystems HeatingSystems { get; set; }
            [XmlElement(ElementName = "HasHotTubSpa", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasHotTubSpa { get; set; }
            [XmlElement(ElementName = "Intercom", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Intercom { get; set; }
            [XmlElement(ElementName = "HasJettedBathTub", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasJettedBathTub { get; set; }
            [XmlElement(ElementName = "HasLawn", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasLawn { get; set; }
            [XmlElement(ElementName = "LegalDescription", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string LegalDescription { get; set; }
            [XmlElement(ElementName = "HasMotherInLaw", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasMotherInLaw { get; set; }
            [XmlElement(ElementName = "IsNewConstruction", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string IsNewConstruction { get; set; }
            [XmlElement(ElementName = "NumFloors", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string NumFloors { get; set; }
            [XmlElement(ElementName = "NumParkingSpaces", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string NumParkingSpaces { get; set; }
            [XmlElement(ElementName = "HasPatio", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasPatio { get; set; }
            [XmlElement(ElementName = "HasPond", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasPond { get; set; }
            [XmlElement(ElementName = "HasPool", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasPool { get; set; }
            [XmlElement(ElementName = "HasPorch", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasPorch { get; set; }
            [XmlElement(ElementName = "RoofTypes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public RoofTypes RoofTypes { get; set; }
            [XmlElement(ElementName = "RoomCount", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string RoomCount { get; set; }
            [XmlElement(ElementName = "Rooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Rooms Rooms { get; set; }
            [XmlElement(ElementName = "HasRVParking", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasRVParking { get; set; }
            [XmlElement(ElementName = "HasSauna", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasSauna { get; set; }
            [XmlElement(ElementName = "HasSecuritySystem", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasSecuritySystem { get; set; }
            [XmlElement(ElementName = "HasSkylight", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasSkylight { get; set; }
            [XmlElement(ElementName = "HasSportsCourt", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasSportsCourt { get; set; }
            [XmlElement(ElementName = "HasSprinklerSystem", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasSprinklerSystem { get; set; }
            [XmlElement(ElementName = "HasVaultedCeiling", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasVaultedCeiling { get; set; }
            [XmlElement(ElementName = "ViewTypes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ViewTypes ViewTypes { get; set; }
            [XmlElement(ElementName = "IsWaterfront", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string IsWaterfront { get; set; }
            [XmlElement(ElementName = "HasWetBar", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HasWetBar { get; set; }
            [XmlElement(ElementName = "IsWired", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string IsWired { get; set; }
            [XmlElement(ElementName = "YearUpdated", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string YearUpdated { get; set; }
        }

        [XmlRoot(ElementName = "ModificationTimestamp", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class ModificationTimestamp
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Disclaimer", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Disclaimer
        {
            [XmlAttribute(AttributeName = "isgSecurityClass", Namespace = "http://rets.org/xsd/RETSCommons")]
            public string IsgSecurityClass { get; set; }
            [XmlText]
            public string Text { get; set; }
        }

        [XmlRoot(ElementName = "Listing", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Listing
        {
            [XmlElement(ElementName = "Address", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Address Address { get; set; }
            [XmlElement(ElementName = "ListPrice", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ListPrice ListPrice { get; set; }
            [XmlElement(ElementName = "ListPriceLow", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ListPriceLow ListPriceLow { get; set; }
            [XmlElement(ElementName = "AlternatePrices", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public AlternatePrices AlternatePrices { get; set; }
            [XmlElement(ElementName = "ListingURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingURL { get; set; }
            [XmlElement(ElementName = "ProviderName", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ProviderName { get; set; }
            [XmlElement(ElementName = "ProviderURL", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ProviderURL { get; set; }
            [XmlElement(ElementName = "ProviderCategory", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ProviderCategory { get; set; }
            [XmlElement(ElementName = "LeadRoutingEmail", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string LeadRoutingEmail { get; set; }
            [XmlElement(ElementName = "Bedrooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Bedrooms { get; set; }
            [XmlElement(ElementName = "Bathrooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string Bathrooms { get; set; }
            [XmlElement(ElementName = "PropertyType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public PropertyType PropertyType { get; set; }
            [XmlElement(ElementName = "PropertySubType", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public PropertySubType PropertySubType { get; set; }
            [XmlElement(ElementName = "ListingKey", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingKey { get; set; }
            [XmlElement(ElementName = "ListingCategory", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingCategory { get; set; }
            [XmlElement(ElementName = "ListingStatus", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingStatus { get; set; }
            [XmlElement(ElementName = "MarketingInformation", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public MarketingInformation MarketingInformation { get; set; }
            [XmlElement(ElementName = "Photos", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Photos Photos { get; set; }
            [XmlElement(ElementName = "DiscloseAddress", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string DiscloseAddress { get; set; }
            [XmlElement(ElementName = "ListingDescription", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingDescription { get; set; }
            [XmlElement(ElementName = "MlsId", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MlsId { get; set; }
            [XmlElement(ElementName = "MlsName", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MlsName { get; set; }
            [XmlElement(ElementName = "MlsNumber", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string MlsNumber { get; set; }
            [XmlElement(ElementName = "LivingArea", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string LivingArea { get; set; }
            [XmlElement(ElementName = "LotSize", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string LotSize { get; set; }
            [XmlElement(ElementName = "YearBuilt", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string YearBuilt { get; set; }
            [XmlElement(ElementName = "ListingDate", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingDate { get; set; }
            [XmlElement(ElementName = "ListingTitle", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ListingTitle { get; set; }
            [XmlElement(ElementName = "FullBathrooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string FullBathrooms { get; set; }
            [XmlElement(ElementName = "ThreeQuarterBathrooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ThreeQuarterBathrooms { get; set; }
            [XmlElement(ElementName = "HalfBathrooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string HalfBathrooms { get; set; }
            [XmlElement(ElementName = "OneQuarterBathrooms", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string OneQuarterBathrooms { get; set; }
            [XmlElement(ElementName = "ForeclosureStatus", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public string ForeclosureStatus { get; set; }
            [XmlElement(ElementName = "ListingParticipants", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ListingParticipants ListingParticipants { get; set; }
            [XmlElement(ElementName = "VirtualTours", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public VirtualTours VirtualTours { get; set; }
            [XmlElement(ElementName = "Videos", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Videos Videos { get; set; }
            [XmlElement(ElementName = "Offices", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Offices Offices { get; set; }
            [XmlElement(ElementName = "Brokerage", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Brokerage Brokerage { get; set; }
            [XmlElement(ElementName = "Franchise", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Franchise Franchise { get; set; }
            [XmlElement(ElementName = "Builder", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Builder Builder { get; set; }
            [XmlElement(ElementName = "Location", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Location Location { get; set; }
            [XmlElement(ElementName = "OpenHouses", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public OpenHouses OpenHouses { get; set; }
            [XmlElement(ElementName = "Taxes", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Taxes Taxes { get; set; }
            [XmlElement(ElementName = "Expenses", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Expenses Expenses { get; set; }
            [XmlElement(ElementName = "DetailedCharacteristics", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public DetailedCharacteristics DetailedCharacteristics { get; set; }
            [XmlElement(ElementName = "ModificationTimestamp", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public ModificationTimestamp ModificationTimestamp { get; set; }
            [XmlElement(ElementName = "Disclaimer", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Disclaimer Disclaimer { get; set; }
        }

        [XmlRoot(ElementName = "Listings", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
        public class Listings
        {
            [XmlElement(ElementName = "SingleListing", Namespace = "http://rets.org/xsd/Syndication/2012-03")]
            public Listing Listing { get; set; }
            [XmlAttribute(AttributeName = "xmlns")]
            public string Xmlns { get; set; }
            [XmlAttribute(AttributeName = "commons", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Commons { get; set; }
            [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string SchemaLocation { get; set; }
            [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
            public string Xsi { get; set; }
            [XmlAttribute(AttributeName = "listingsKey")]
            public string ListingsKey { get; set; }
            [XmlAttribute(AttributeName = "version")]
            public string Version { get; set; }
            [XmlAttribute(AttributeName = "versionTimestamp")]
            public string VersionTimestamp { get; set; }
            [XmlAttribute(AttributeName = "lang", Namespace = "http://www.w3.org/XML/1998/namespace")]
            public string Lang { get; set; }

            [XmlElement("Listing")]
            public List<Listing> AllListing { get; set; }
        }

        public class ListingObject
        {
            public int Id { get; set; }
            public string Versionstamp { get; set; }
            public string Version { get; set; }
            public string Listingskey { get; set; }
            public string ListingKey { get; set; }
            public string ModificationTimestampText { get; set; }
            public DateTime ModificationTimestampDate { get; set; }
            public string ListingStatus { get; set; }
            public string MlsId { get; set; }
            public string MlsName { get; set; }
            public string MlsNumber { get; set; }
            public byte[] listingXmlCompressed { get; set; }
            public bool doAllProcessing { get; set; }
            public long feedID { get; set; }
            public string FullStreetAddress { get; set; }
            public string City { get; set; }
            public string StateOrProvince { get; set; }
            public string PostalCode { get; set; }
            public string County { get; set; }
            public string path { get; set; }
            public string IncomingStatus { get; set; }

            public ListingObject()
            {
            }
        }

        public class ListingCompareObject
        {
            public int Id { get; set; }
            public string ListingKey { get; set; }
            public string ModificationTimestamp { get; set; }
            public DateTime ModificationTimestampDate { get; set; }
            public string StateOrProvince { get; set; }

        }
    }
}
