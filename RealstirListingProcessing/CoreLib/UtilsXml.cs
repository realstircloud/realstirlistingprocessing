﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Realstir.Engine.Listing.Core
{
    public class UtilsXml
    {
        //public static bool Serialize<T>(T value, ref string serializeXml)
        //{
        //    bool bReturn = false;

        //    if (value != null)
        //    {
        //        try
        //        {
        //            XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
        //            StringWriter stringWriter = new StringWriter();
        //            XmlWriter writer = XmlWriter.Create(stringWriter);
        //            xmlserializer.Serialize(writer, value);
        //            serializeXml = stringWriter.ToString();
        //            writer.Close();
        //            bReturn = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            bReturn = false;

        //        }
        //    }

        //    return bReturn;
        //}

        //This will returns the set of included namespaces for the serializer.
        public static XmlSerializerNamespaces GetNamespaces()
        {

            XmlSerializerNamespaces ns;
            ns = new XmlSerializerNamespaces();
            ns.Add("xs", "http://www.w3.org/2001/XMLSchema");
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            return ns;
        }

        //Returns the target namespace for the serializer.
        public static string TargetNamespace
        {
            get { return "http://www.w3.org/2001/XMLSchema"; }
        }
        public static string SerializeObject(object obj)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                serializer.Serialize(ms, obj);
                ms.Position = 0;
                xmlDoc.Load(ms);
                return xmlDoc.InnerXml;
            }
        }

        //Creates an object from an XML string.
        public static object FromXml(string Xml, System.Type ObjType)
        {

            XmlSerializer ser;
            ser = new XmlSerializer(ObjType);
            StringReader stringReader;
            stringReader = new StringReader(Xml);
            XmlTextReader xmlReader;
            xmlReader = new XmlTextReader(stringReader);
            object obj;
            obj = ser.Deserialize(xmlReader);
            xmlReader.Close();
            stringReader.Close();
            return obj;

        }

        //Serializes the <i>Obj</i> to an XML string.
        public static string ToXml(object Obj, System.Type ObjType)
        {

            XmlSerializer ser;
            ser = new XmlSerializer(ObjType, UtilsXml.TargetNamespace);
            MemoryStream memStream;
            memStream = new MemoryStream();
            XmlTextWriter xmlWriter;
            xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);
            xmlWriter.Namespaces = true;
            ser.Serialize(xmlWriter, Obj, UtilsXml.GetNamespaces());
            xmlWriter.Close();
            memStream.Close();
            string xml;
            xml = Encoding.UTF8.GetString(memStream.GetBuffer());
            xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)));
            xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1));
            return xml;

        }

        public static void RemoveAttributes(XElement xRoot)
        {
            foreach (var xAttr in xRoot.Attributes().ToList())
                xAttr.Remove();

            foreach (var xElem in xRoot.Descendants())
                RemoveAttributes(xElem);
        }

        public static XElement stripNS(XElement root)
        {
            return new XElement(
                root.Name.LocalName,
                root.HasElements ?
                    root.Elements().Select(el => stripNS(el)) :
                    (object)root.Value
            );
        }

        public static string ConditionHomeJunctionXML(string data)
        {
            StringBuilder sb = new StringBuilder(data);
            string s1 = "xmlns=\"http://rets.org/xsd/Syndication/2012-03\"";
            string s2 = "xmlns:commons=\"http://rets.org/xsd/RETSCommons\"";

            //string p1 = "<Listing>";
            string p2 = "<commons:FullStreetAddress";
            string p3 = "<commons:City";
            string p4 = "<commons:StateOrProvince";
            string p5 = "<commons:PostalCode";
            string p6 = "<commons:Subdivision";
            string p7 = "<commons:Schools";


            if (!string.IsNullOrEmpty(data))
            {
                //returnData = returnData.Replace(p1, "<Listing " + s1 + ">");
                sb = sb.Replace(p2, p2 + " " + s2);
                sb = sb.Replace(p3, p3 + " " + s2);
                sb = sb.Replace(p4, p4 + " " + s2);
                sb = sb.Replace(p5, p5 + " " + s2);
                sb = sb.Replace(p6, p6 + " " + s2);
                sb = sb.Replace(p7, p7 + " " + s2);
            }
            return sb.ToString();
        }
    }
}
