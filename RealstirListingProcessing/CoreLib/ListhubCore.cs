﻿using System;

using Realstir.Engine.Listing.Db.DAL;
using System.Collections.Generic;
using System.Net;

namespace Realstir.Engine.Listing.Core
{
    public class ListhubCore
    {
        private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStacktrace = null;

        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
        public ListhubCore()
        {
        }
        public void DownloadFeed()
        {
            Feed feed = null;
            try
            {
                string baseurl = new Sysconfig("SYSTEM_CONFIGURATION_LISTHUB_URI").ConfigValue;
                string username = new Sysconfig("SYSTEM_CONFIGURATION_LISTHUB_USERNAME").ConfigValue;
                string password = new Sysconfig("SYSTEM_CONFIGURATION_LISTHUB_PASSWORD").ConfigValue;
                string channelID = new Sysconfig("SYSTEM_CONFIGURATION_LISTHUB_CHANNELID").ConfigValue;
                string filename = new Sysconfig("SYSTEM_CONFIGURATION_LISTHUB_FILENAME").ConfigValue;

                string url = baseurl + channelID + "/" + filename;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.Method = "HEAD";
                request.Credentials = new NetworkCredential(username, password);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // you can check headers before continuing:
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    _hasError = true;
                    response.Close();
                    return;
                }
                //Console.WriteLine(response.Headers.ToString());
                string headerHttpTime = response.Headers.Get("Last-Modified");
                string headerContentType = response.Headers.Get("Content-Type");
                string headerContentLength = response.Headers.Get("Content-Length");
                string headerDate = response.Headers.Get("Date");
                int nHeaderContentLength = Convert.ToInt32(headerContentLength);
                response.Close();
                response = null;

                DateTime httpHeaderLastModifiedDateToLocal = DateTime.Parse(headerHttpTime);
                DateTime httpHeaderLastModifiedDateUTC = DateTime.Parse(headerHttpTime).ToUniversalTime();

                var fileDate = httpHeaderLastModifiedDateUTC.ToString("dd-MMM-yyyy-HH-mm-ss");
                //string saveAs = string.Format("{0}_{1}", fileDate, filename);
                string saveAsFilename = string.Format("{0}_{1}_{2}",
                                    "Listhub",
                                    fileDate, filename);
                List<Feed> lstFeed = null;
                feed = new Feed();
                lstFeed = feed.Load("WHERE incoming_filename = " + saveAsFilename);
                if (lstFeed.Count == 0)
                {

                }
            }
            catch (Exception e)
            {
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;

            }
            finally
            {
                if (_hasError)
                {
                    try
                    {
                        //Syslog syslog = new Syslog();
                        //syslog.Msgaction = "ERROR_DOWNLOAD_LISTHUB";
                        //syslog.Msgsource = "DownloadListhubFeed";
                        //syslog.Msgtxt = _errorMessage + " " + _errorStacktrace;
                        //long syslogID = _busFacCore.SyslogCreateOrModify(syslog);
                    }
                    catch { }
                }
            }
        }
    }
}
